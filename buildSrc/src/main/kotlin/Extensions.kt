import org.apache.tools.ant.filters.ReplaceTokens
import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.credentials.HttpHeaderCredentials
import org.gradle.api.file.DuplicatesStrategy
import org.gradle.api.tasks.TaskCollection
import org.gradle.authentication.http.HttpHeaderAuthentication
import org.gradle.kotlin.dsl.create
import org.gradle.kotlin.dsl.credentials
import org.gradle.kotlin.dsl.filter
import org.gradle.kotlin.dsl.repositories
import org.gradle.language.jvm.tasks.ProcessResources
import org.jetbrains.kotlin.gradle.plugin.KotlinDependencyHandler
import java.io.File
import java.net.URI
import java.util.concurrent.TimeUnit

fun Project.runCommand(
    cmd: String,
    workingDir: File = project.projectDir,
    timeoutAmount: Long = 60,
    timeoutUnit: TimeUnit = TimeUnit.SECONDS
): String = ProcessBuilder(cmd.split("\\s(?=(?:[^'\"`]*(['\"`])[^'\"`]*\\1)*[^'\"`]*$)".toRegex()))
    .directory(workingDir)
    .redirectOutput(ProcessBuilder.Redirect.PIPE)
    .redirectError(ProcessBuilder.Redirect.PIPE)
    .start()
    .apply {
        waitFor(timeoutAmount, timeoutUnit)
    }
    .run {
        val errorText = errorStream.bufferedReader().readText().trim()
        if (errorText.isNotEmpty()) {
            error(errorText)
        }
        inputStream.bufferedReader().readText().trim()
    }

fun Project.withGitProperties() {
    var value = findProperty("git.commit") as String
    if (value == "unknown") {
        value = runCommand("git rev-parse --short=8 HEAD", rootDir)
        setProperty("git.commit", value)
    }
}

fun Project.repositories() {
    repositories {
        mavenLocal()
        mavenCentral()
    }
}

fun RepositoryHandler.gitlab(uri: URI, auth: String) {
    maven {
        url = uri
        when (auth) {
            "gitlab" -> {
                credentials(HttpHeaderCredentials::class) {
                    System.getenv("GITLAB_PRIVATE_TOKEN")?.let {
                        name = "Private-Token"
                        value = it
                    } ?: System.getenv("GITLAB_DEPLOY_TOKEN")?.let {
                        name = "Deploy-Token"
                        value = it
                    } ?: System.getenv("CI_JOB_TOKEN")?.let {
                        name = "Job-Token"
                        value = it
                    }
                }
                authentication {
                    create<HttpHeaderAuthentication>("header")
                }
            }
        }
    }
}

fun Project.filterResources(tasks: TaskCollection<ProcessResources>) = tasks.forEach {
    filterResources(it)
}

fun Project.filterResources(processResources: ProcessResources) = with (processResources) {
    filesMatching("**/*.properties") {
        filter<ReplaceTokens>("tokens" to properties.filter { it.value is String })
    }
    duplicatesStrategy = DuplicatesStrategy.WARN
}

fun KotlinDependencyHandler.jacksonDependencies(version: String) {
    listOf(
        "com.fasterxml.jackson.core:jackson-core",
        "com.fasterxml.jackson.core:jackson-databind",
        "com.fasterxml.jackson.core:jackson-annotations",
        "com.fasterxml.jackson.module:jackson-module-kotlin",
        "com.fasterxml.jackson.dataformat:jackson-dataformat-xml",
        "com.fasterxml.jackson.dataformat:jackson-dataformat-yaml",
        "com.fasterxml.jackson.jaxrs:jackson-jaxrs-xml-provider",
        "com.fasterxml.jackson.datatype:jackson-datatype-jsr310"
    ).map { implementation("$it:$version") }
}
