import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "org.kodebase"

val kotlinVersion = findProperty("kotlin.version") as String

plugins {
    kotlin("jvm")
    id("maven-publish")
    id("org.jetbrains.dokka")
}

withGitProperties()

repositories()

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlinVersion")
}

val sourcesJar by tasks.registering(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

publishing {
    publications {
        register("mavenJava", MavenPublication::class) {
            from(components["java"])
            artifact(sourcesJar.get())
        }
    }
    repositories {
        gitlab(
            uri(findProperty("maven.repository.uri") as String),
            findProperty("maven.repository.auth") as String
        )
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = JavaVersion.VERSION_11.toString()
}

filterResources(tasks.withType())
