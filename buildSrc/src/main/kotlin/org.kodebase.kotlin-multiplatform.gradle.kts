plugins {
    kotlin("multiplatform")
    id("maven-publish")
    id("java-library")
    id("org.jetbrains.dokka")
}

val kotlinVersion = findProperty("kotlin.version") as String

withGitProperties()

repositories()

kotlin {
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = JavaVersion.VERSION_11.toString()
        }
        testRuns["test"].executionTask.configure {
            useJUnit()
        }
        withJava()
    }
//    js(LEGACY) {
//        binaries.executable()
//        browser {
//            commonWebpackConfig {
//                cssSupport.enabled = true
//            }
//        }
//    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                //implementation("io.ktor:ktor-client-logging:$ktorVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.1")
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.3.2")
                implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlinVersion")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }
        val jvmMain by getting {
            dependencies {
            }
        }
        val jvmTest by getting {
            dependencies {
                /*
                implementation("org.jetbrains.kotlin:kotlin-test:$kotlinVersion")
                implementation("org.assertj:assertj-core:3.12.2")
                implementation("org.junit.jupiter:junit-jupiter:5.6.0")
                implementation("io.mockk:mockk:1.10.2") {
                    exclude("org.jetbrains.kotlin", "kotlin-reflect")
                }*/
            }
        }
    }
}

publishing {
    repositories {
        gitlab(
            uri(findProperty("maven.repository.uri") as String),
            findProperty("maven.repository.auth") as String
        )
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

filterResources(tasks.getByName<ProcessResources>("jvmProcessResources"))

tasks.getByName<ProcessResources>("jvmTestProcessResources") {
    duplicatesStrategy = DuplicatesStrategy.WARN
}
