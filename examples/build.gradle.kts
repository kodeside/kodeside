group = "org.kodebase"
description = "Kodebase Examples"

val slf4jVersion = findProperty("slf4j.version")
val kotlinxCliVersion = findProperty("kotlinx-cli.version")
val ktorVersion = findProperty("ktor.version")
val kodeinVersion = findProperty("kodein.version")
val kmongoVersion = findProperty("kmongo.version")
val kompendiumVersion = findProperty("kompendium.version")
val kgraphqlVersion = findProperty("kgraphql.version")

plugins {
    id("org.kodebase.kotlin-multiplatform")
    kotlin("plugin.serialization") version "1.8.20"
}

kotlin {
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(project(":kodebase-lib"))
                implementation("org.jetbrains.kotlinx:kotlinx-cli:$kotlinxCliVersion")

            }
        }
        val jvmMain by getting {
            dependencies {
                //implementation("io.ktor:ktor-server-core:$ktorVersion")
                implementation("org.slf4j:slf4j-simple:$slf4jVersion")
                implementation("org.kodein.di:kodein-di-conf-jvm:$kodeinVersion")
                implementation("io.ktor:ktor-server-netty:$ktorVersion")
                implementation("io.ktor:ktor-server-content-negotiation:$ktorVersion")
                implementation("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")
                implementation("io.ktor:ktor-server-auth-jwt:$ktorVersion")
                implementation("io.ktor:ktor-server-openapi:$ktorVersion")
                implementation("io.ktor:ktor-server-swagger:$ktorVersion")
                implementation("org.apache.activemq:activemq-client:5.17.1")
                //implementation("io.confluent:kafka-jms-client:7.2.0")
                implementation("org.litote.kmongo:kmongo-serialization:$kmongoVersion")
                implementation("com.apurebase:kgraphql-ktor:$kgraphqlVersion")
            }
        }
    }
}
