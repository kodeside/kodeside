// Example1: Simple server application
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.kodebase.application.*

fun main() {
    server {
        routing {
            get("/") {
                call.respond("Hello World!")
            }
        }
    }
}