// Example2: Server application using DI
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.datetime.*
import org.kodebase.application.*
import org.kodebase.di.di
import org.kodein.di.*

fun main() {
    di {
        bindSingleton { { Clock.System.now() } }
    }
    server {
        val clock: () -> Instant by di.instance()
        routing {
            get("/") {
                val time = clock()
                call.respond("Current time is $time")
            }
        }
    }
}