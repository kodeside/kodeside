// Example3: Application with CLI (Command Line Interface), using config
@file:OptIn(ExperimentalCli::class)

import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.cli.*
import org.kodebase.common.cli.*
import org.kodebase.common.config
import org.kodebase.config.getTyped
import org.kodebase.di.*
import org.kodein.di.*

class MyComponent {
    fun greeting(nick: String?) = "Hello ${nick ?: "Anonym"}!"
}

class MySubcommand(
    private val component: MyComponent
) : Subcommand("greet", "Greet someone") {

    private val nick by argument(ArgType.String, "nick", "Nick").optional()

    override fun execute() {
        println(component.greeting(nick))
    }
}

/**
 * Try to run me with arguments
 *
 * server -h
 * server
 * greet -h
 * greet John
 */
fun main(args: Array<String>) {
    di {
        bindSingleton { new(::MyComponent) }
        bindSingleton<CliCommand> { MainCommand(config.getTyped("app.name"), instance()) }
        bindSet<Subcommand>()
        inSet<Subcommand> {
            singleton {
                ServerSubcommand(port = config.getTyped("server.port")) {
                    val component by di.instance<MyComponent>()
                    routing {
                        get("/") {
                            call.respond(component.greeting(call.parameters["nick"]))
                        }
                    }
                }
            }
        }
        inSet<Subcommand> { singleton { new(::MySubcommand) } }
    }
    val command by di.instance<CliCommand>()
    command.parse(args)
}