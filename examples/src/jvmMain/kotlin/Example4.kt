// Example4: Server application with JSON and authorization support
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.Serializable
import org.kodebase.application.*
import org.kodebase.application.security.*

@Serializable
data class Person(val name: String)

fun Application.installAuthentication() = install(Authentication) {
    basic("basic") {
        realm = "Example"
        validate {
            if (it.name == "test" && it.password == "test")
                NamedIdentity(it.name)
            else
                null
        }
    }
}

val persons = mutableListOf(Person("John"), Person("Fred"))

/**
 * Try
 * curl -X POST http://localhost:8070 --user test:test -H 'Content-Type: application/json' -d '{"name":"Jack"}' -v
 */
fun main() {
    server(port = 8080) {
        installAuthentication()
        install(ContentNegotiation) {
            json()
        }
        routing {
            //openAPI(path="openapi", swaggerFile = "openapi/documentation.yaml")
            //swaggerUI("swagger", "/openapi.json", "")
            route("/") {
                get("/") {
                    call.respond(persons)
                }
                authenticate("basic") {
                    post("/") {
                        val person = call.receive<Person>()
                        persons.add(person)
                        println("Caller identity ${call.identity} added $person")
                        call.respond(person)
                    }
                }
            }
        }
    }
}