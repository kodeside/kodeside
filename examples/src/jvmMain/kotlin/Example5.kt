// Example5: Server application with GraphQL
import com.apurebase.kgraphql.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import kotlinx.serialization.Serializable
import org.kodebase.application.*
import org.kodebase.application.security.*
import org.kodebase.graphql.*

@Serializable
data class AddPerson(val name: String)

fun Application.installGraphQL() = install(GraphQLLazy) {
    playground = true
    wrapErrors = false
    useDefaultPrettyPrinter = true
    wrap {
        authenticate("basic", optional = true, build = it)
    }
    context { call ->
        call.authentication.principal<PrincipalIdentity>()?.let { +it }
    }
}

/**
 * Try http://localhost:8071/graphql
 */
fun main() {
    server(8071) {
        installAuthentication()
        installGraphQL()

        schema {
            query("persons") {
                resolver { -> persons }
            }
            mutation("addPerson") {
                resolver { input: AddPerson, context: Context ->
                    val person = Person(input.name)
                    persons.add(person)
                    println("Caller identity ${context.identity} added $person")
                    person
                }
            }
        }
    }
}