package org.kodebase.examples

import org.kodein.di.instance
import org.kodebase.common.cli.CliCommand
import org.kodebase.di.di

fun main(args: Array<String>) {
    bindings()
    val command by di.instance<CliCommand>()
    command.parse(args)
}