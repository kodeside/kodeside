package org.kodebase.examples

import kotlinx.cli.ExperimentalCli
import kotlinx.cli.Subcommand
import org.apache.activemq.ActiveMQConnectionFactory
import org.kodein.di.*
import org.kodebase.common.cli.CliCommand
import org.kodebase.common.cli.MainCommand
import org.kodebase.common.cli.ServerSubcommand
import org.kodebase.common.cli.VersionSubcommand
import org.kodebase.common.config
import org.kodebase.common.job.HeartBeatJob
import org.kodebase.common.job.Job
import org.kodebase.common.job.jobBindings
import org.kodebase.config.getTyped
import org.kodebase.di.di
import org.kodebase.di.new
import org.kodebase.examples.cli.*
import org.kodebase.examples.event.*
import org.kodebase.examples.command.*
import org.kodebase.examples.query.*
import org.kodebase.examples.repository.*
import org.kodebase.messaging.di.*
import org.kodebase.monitoring.LoggerMonitoring
import org.kodebase.monitoring.Monitoring
import org.litote.kmongo.KMongo

@OptIn(ExperimentalCli::class)
fun bindings() {
    val databaseUrl = config.getOrNull("database.url")
    val brokerUrl = config.getOrNull("messaging.brokerURL")
    val name = config.getTyped<String>("app.name")

    di {
        bindSingleton<Monitoring> { LoggerMonitoring }

        if (databaseUrl != null) {
            bindSingleton { KMongo.createClient(databaseUrl).getDatabase("kodebase-$name") }
            bindSingleton { new(::MongoCustomerRepository) }
        } else {
            bindSingleton { new(::MemoryCustomerRepository) }
        }

        /* jobs */
        jobBindings()
        inSet<Job> { singleton { HeartBeatJob } }

        /* messaging */
        if (brokerUrl != null)
            jmsMessaging(ActiveMQConnectionFactory(brokerUrl), serializer)
        else
            localMessaging()
        messageBusLogging()

        commandHandler { singleton { new(::AddCustomerHandler) } }
        queryHandler { singleton { new(::GetCustomersHandler) } }
        eventHandler { singleton { new(::CustomerCreatedHandler) } }

        /* cli */
        bindSingleton<CliCommand> { MainCommand(name, instance()) }
        bindSet<Subcommand>()
        inSet<Subcommand> {
            singleton {
                ServerSubcommand(config.getTyped("server.port")) {
                    features()
                    routing()
                }
            }
        }
        inSet<Subcommand> { singleton { VersionSubcommand } }
        inSet<Subcommand> { singleton { new(::CustomersSubcommand) } }
    }
}