package org.kodebase.examples

import io.ktor.server.application.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json

val serializer = Json {
    prettyPrint = true
}

fun Application.features() {
    install(ContentNegotiation) {
        json(serializer)
    }
//    install(NotarizedApplication()) {
//        spec = OpenApiSpec(
//            openapi = "3.0.0",
//            info = Info(
//                title = "Examples",
//                version = "1.0.0",
//                description = "Generated API spec",
//                termsOfService = URI("https://examples.kodebase.org/terms"),
//            ),
//            servers = mutableListOf(
//                Server(
//                    url = URI("https://examples.kodebase.org"),
//                    description = "Production API"
//                ),
//                Server(
//                    url = URI("http://localhost:8010"),
//                    description = "Local API"
//                ),
//            )
//        )
//        addCustomTypeSchema(ID::class, SimpleSchema("string", format = "ID"))
//        addCustomTypeSchema(Instant::class, SimpleSchema("string", format = "ISO-6801"))
//        addCustomTypeSchema(LocalDate::class, SimpleSchema("string", format = "ISO-6801"))
//    }
//    install(SwaggerUI) {
//        swaggerUrl = "/swagger-ui"
//        jsConfig = JsConfig(specs = mapOf("Examples API V1" to URI("/openapi.json")))
//    }
}