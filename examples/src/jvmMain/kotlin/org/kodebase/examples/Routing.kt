package org.kodebase.examples

import io.ktor.server.application.*
import io.ktor.http.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.kodein.di.instance
import org.kodebase.application.stringParameter
import org.kodebase.common.job.*
import org.kodebase.di.di
import org.kodebase.examples.command.AddCustomer
import org.kodebase.examples.dto.Customer
import org.kodebase.examples.query.GetCustomers
import org.kodebase.messaging.command.CommandBus
import org.kodebase.messaging.query.QueryBus
import org.kodebase.messaging.dispatch

fun Application.routing() {

    val commandBus: CommandBus by di.instance()
    val queryBus: QueryBus by di.instance()

    routing {
        jobRouting()
        get("/") {
//            notarizedGet(
//                GetInfo<Unit, List<Customer>>(
//                    summary = "Get customers",
//                    responseInfo = ResponseInfo(
//                        status = HttpStatusCode.OK,
//                        description = "Customers"
//                    ),
//                )
//            ) {
            val query = GetCustomers(call.stringParameter("q", ""))
            call.respond(queryBus.dispatch(query))
        }

        put("/") {
//            notarizedPut(
//                PutInfo<Unit, AddCustomer, Int>(
//                    summary = "Add customer",
//                    requestInfo = RequestInfo(
//                        description = "Customer"
//                    ),
//                    responseInfo = ResponseInfo(
//                        status = HttpStatusCode.OK,
//                        description = "Number of customers"
//                    ),
//                    //securitySchemes = setOf("jwt-token", "api-key")
//                )
//            ) {
            val command = call.receive<AddCustomer>()
            call.respond(commandBus.dispatch(command))
        }
    }
}
