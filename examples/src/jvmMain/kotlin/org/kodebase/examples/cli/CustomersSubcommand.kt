package org.kodebase.examples.cli

import kotlinx.cli.ArgType
import kotlinx.cli.ExperimentalCli
import kotlinx.cli.Subcommand
import kotlinx.cli.default
import org.kodebase.examples.query.GetCustomers
import org.kodebase.messaging.dispatch
import org.kodebase.messaging.query.QueryBus

@OptIn(ExperimentalCli::class)
class CustomersSubcommand(
    private val queryBus: QueryBus
) : Subcommand("customers", "Work with customers") {

    private val q by option(ArgType.String, "query", "q", "Search query").default("")

    override fun execute() = queryBus.dispatch(GetCustomers(q)).forEach {
        println(it)
    }
}