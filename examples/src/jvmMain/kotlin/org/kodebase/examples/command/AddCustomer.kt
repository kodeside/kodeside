package org.kodebase.examples.command

import kotlinx.serialization.Serializable
import org.kodebase.messaging.command.Command

@Serializable
data class AddCustomer(val name: String) : Command<Int>