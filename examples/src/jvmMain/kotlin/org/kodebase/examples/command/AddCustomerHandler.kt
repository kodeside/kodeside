package org.kodebase.examples.command

import org.kodebase.examples.model.Customer
import org.kodebase.examples.repository.CustomerRepository
import org.kodebase.messaging.Envelope
import org.kodebase.messaging.command
import org.kodebase.messaging.command.CommandHandler
import org.kodebase.messaging.event.EventBus
import org.kodebase.messaging.result

class AddCustomerHandler(
    private val customerRepository: CustomerRepository<*>,
    private val eventBus: EventBus,
) : CommandHandler<AddCustomer, Int> {

    override val canHandle = AddCustomer::class

    override fun handle(envelope: Envelope<AddCustomer, Int>) = result {
        val customer = Customer.create(envelope.command.name)
        customerRepository.save(customer)
        eventBus.dispatch(customer)
        customerRepository.count
    }
}