package org.kodebase.examples.dto

import org.kodebase.common.type.ID
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class Customer(
    val id: ID,
    val name: String,
    val created: Instant
)
