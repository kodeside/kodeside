package org.kodebase.examples.event

import kotlinx.serialization.Serializable
import org.kodebase.common.type.ID
import org.kodebase.messaging.event.DomainEvent

@Serializable
data class CustomerCreated(val id: ID) : DomainEvent