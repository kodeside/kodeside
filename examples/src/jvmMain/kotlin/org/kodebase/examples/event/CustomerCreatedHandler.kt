package org.kodebase.examples.event

import org.kodebase.messaging.Envelope
import org.kodebase.messaging.event
import org.kodebase.messaging.event.EventHandler
import org.kodebase.messaging.result

class CustomerCreatedHandler : EventHandler<CustomerCreated> {

    override val canHandle = CustomerCreated::class

    override fun handle(envelope: Envelope<CustomerCreated, Unit>) = result {
        println(envelope.event)
    }
}