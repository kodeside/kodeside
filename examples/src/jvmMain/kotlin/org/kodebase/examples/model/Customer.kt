package org.kodebase.examples.model

import org.kodebase.common.AggregateRoot
import org.kodebase.common.type.newId
import org.kodebase.examples.dto.Customer as CustomerDto
import org.kodebase.examples.event.CustomerCreated
import kotlinx.datetime.Clock.System.now
import kotlinx.serialization.Serializable

@Serializable
class Customer private constructor(
    val name: String,
) : AggregateRoot() {
    override val _id = newId()

    val created = now()

    val dto get() = CustomerDto(_id, name, created)

    companion object {
        fun create(name: String) = Customer(name).apply {
            addEvent(CustomerCreated(_id))
        }
    }
}