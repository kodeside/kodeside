package org.kodebase.examples.query

import kotlinx.serialization.Serializable
import org.kodebase.messaging.query.Query
import org.kodebase.examples.dto.Customer

@Serializable
data class GetCustomers(val q: String) : Query<List<Customer>>