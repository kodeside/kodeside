package org.kodebase.examples.query

import org.kodebase.common.repository.MongoEntityRepository
import org.kodebase.examples.model.Customer
import org.kodebase.examples.dto.Customer as CustomerDto
import org.kodebase.examples.repository.CustomerRepository
import org.kodebase.messaging.Envelope
import org.kodebase.messaging.query
import org.kodebase.messaging.query.QueryHandler
import org.kodebase.messaging.result
import org.litote.kmongo.regex

class GetCustomersHandler(
    private val customerRepository: CustomerRepository<Any>
) : QueryHandler<GetCustomers, List<CustomerDto>> {

    override val canHandle = GetCustomers::class

    override fun handle(envelope: Envelope<GetCustomers, List<CustomerDto>>) = result {
        customerRepository.find(envelope.query.filter).map { it.dto }
    }

    private val GetCustomers.filter: Any get() = when(customerRepository) {
        is MongoEntityRepository<*> ->
            Customer::name regex q
        else -> { customer: Customer ->
            customer.name.contains(q.toRegex())
        }
    }
}