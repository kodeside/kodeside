package org.kodebase.examples.repository

import org.kodebase.common.repository.EntityRepository
import org.kodebase.examples.model.Customer

interface CustomerRepository<F> : EntityRepository<Customer, F>