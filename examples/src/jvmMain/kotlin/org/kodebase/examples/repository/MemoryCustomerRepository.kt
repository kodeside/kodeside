package org.kodebase.examples.repository

import org.kodebase.common.repository.MemoryEntityRepository
import org.kodebase.examples.model.Customer

class MemoryCustomerRepository : CustomerRepository<(Customer) -> Boolean>, MemoryEntityRepository<Customer>() {

    init {
        save(Customer.create("Default Customer"))
    }
}