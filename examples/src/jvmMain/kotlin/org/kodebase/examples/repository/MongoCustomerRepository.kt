package org.kodebase.examples.repository

import com.mongodb.client.MongoDatabase
import org.bson.conversions.Bson
import org.kodebase.common.repository.MongoEntityRepository
import org.kodebase.examples.model.Customer

class MongoCustomerRepository(database: MongoDatabase) :
    CustomerRepository<Bson>, MongoEntityRepository<Customer>(database, Customer::class)