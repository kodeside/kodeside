group = "org.kodebase"
description = "Kodebase Lib"

val slf4jVersion = findProperty("slf4j.version")
val kotlinxCliVersion = findProperty("kotlinx-cli.version")
val ktorVersion = findProperty("ktor.version")
val kodeinVersion = findProperty("kodein.version")
val kmongoVersion = findProperty("kmongo.version")
val sentryVersion = findProperty("sentry.version")
val jacksonVersion = findProperty("jackson.version") as String
val okHttpVersion = findProperty("okhttp.version")
val kgraphqlVersion = findProperty("kgraphql.version")


plugins {
    id("org.kodebase.kotlin-multiplatform")
    kotlin("plugin.serialization") version "1.8.20"
}

kotlin {
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-cli:$kotlinxCliVersion")
                implementation("org.kodein.di:kodein-di-conf:$kodeinVersion")
            }
        }
        val jvmMain by getting {
            dependencies {
                //implementation("org.slf4j:slf4j-api:$slf4jVersion")
                implementation("org.slf4j:slf4j-api:$slf4jVersion")
                implementation("io.ktor:ktor-server-auth:$ktorVersion")
                implementation("io.ktor:ktor-server-netty:$ktorVersion")
                implementation("org.litote.kmongo:kmongo-serialization:$kmongoVersion")
                //implementation("org.litote.kmongo:kmongo-id-serialization:$kmongoVersion")
                implementation("io.sentry:sentry:$sentryVersion")
                implementation("javax.jms:javax.jms-api:2.0.1")
                jacksonDependencies(jacksonVersion)
                api("org.litote.kmongo:kmongo-id-jackson:$kmongoVersion")
                api("com.squareup.okhttp3:okhttp:$okHttpVersion")
                implementation("com.apurebase:kgraphql-ktor:$kgraphqlVersion")
                implementation("software.amazon.awssdk:s3:2.17.99")
            }
        }
    }
}
