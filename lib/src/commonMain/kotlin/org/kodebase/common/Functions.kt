@file:OptIn(ExperimentalContracts::class)
package org.kodebase.common

import org.kodebase.common.exception.*
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

fun exception(message: String, vararg argument: Any): Nothing = throw ApplicationException(message, null, null, *argument)

fun applicationConstraint(value: Boolean) {
    contract {
        returns() implies value
    }
    applicationConstraint(value) { "Application constraint failed." }
}

inline fun applicationConstraint(value: Boolean, lazyMessage: () -> Any) {
    contract {
        returns() implies value
    }
    if (!value) {
        val message = lazyMessage()
        throw ApplicationException(message.toString())
    }
}


fun domainConstraint(value: Boolean) {
    contract {
        returns() implies value
    }
    domainConstraint(value) { "Domain constraint failed." }
}

inline fun domainConstraint(value: Boolean, lazyMessage: () -> Any) {
    contract {
        returns() implies value
    }
    if (!value) {
        val message = lazyMessage()
        throw DomainException(message.toString())
    }
}
