package org.kodebase.common.cli

import kotlinx.cli.ArgParser
import kotlinx.cli.ExperimentalCli
import kotlinx.cli.Subcommand

@OptIn(ExperimentalCli::class)
abstract class CliCommand(name: String) {

    abstract val subcommands: Set<Subcommand>

    private val argParser by lazy {
        ArgParser(name).apply {
            subcommands(*subcommands.toTypedArray())
        }
    }

    open fun parse(args: Array<String>) = argParser.parse(if (args.isEmpty()) arrayOf("-h") else args)

    fun parse(line: String) = parse(line.split(' ').toTypedArray())
}