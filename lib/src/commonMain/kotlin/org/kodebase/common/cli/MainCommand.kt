package org.kodebase.common.cli

import kotlinx.cli.ExperimentalCli
import kotlinx.cli.Subcommand

@OptIn(ExperimentalCli::class)
class MainCommand(name: String, override val subcommands: Set<Subcommand>) : CliCommand(name)