package org.kodebase.common.cli

import kotlinx.cli.ExperimentalCli
import kotlinx.cli.Subcommand
import org.kodebase.common.config

@OptIn(ExperimentalCli::class)
object VersionSubcommand : Subcommand("version", "Show application version") {

    override fun execute() = println("${config.getOrDefault("app.name", "unknown")} version ${config.getOrDefault("app.version", "unspecified")}")
}