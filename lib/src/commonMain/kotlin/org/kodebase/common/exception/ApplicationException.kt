package org.kodebase.common.exception

open class ApplicationException(
    override val message: String,
    val language: String? = null,
    override val cause: Throwable? = null,
    vararg argument: Any
) : Exception() {
    val arguments = argument

    override fun toString() = "${this::class.qualifiedName}(message=$message, language=$language, arguments=${arguments.toList()})"
}