package org.kodebase.common.exception

import org.kodebase.common.type.ID

open class DomainException(
    override val message: String? = null,
    override val cause: Throwable? = null,
    open val entityId: ID? = null,
) : Throwable()