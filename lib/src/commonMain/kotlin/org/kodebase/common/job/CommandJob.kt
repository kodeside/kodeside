package org.kodebase.common.job

import org.kodebase.messaging.command.Command

interface CommandJob<R : Any> : Job {

    val command: Command<R>?
}