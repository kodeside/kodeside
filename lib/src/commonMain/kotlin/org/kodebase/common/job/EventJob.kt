package org.kodebase.common.job

import org.kodebase.messaging.event.Event

interface EventJob : Job {

    val event: Event?
}