package org.kodebase.common.job

fun at(month: Int = -1, day: Int = -1, dayOfWeek: Int = -1, hour: Int = -1, minute: Int = -1) =
    JobSchedule(JobScheduleType.At, month, day, dayOfWeek, hour, minute)

fun every(month: Int = -1, day: Int = -1, dayOfWeek: Int = -1, hour: Int = -1, minute: Int = -1) =
    JobSchedule(JobScheduleType.Every, month, day, dayOfWeek, hour, minute)