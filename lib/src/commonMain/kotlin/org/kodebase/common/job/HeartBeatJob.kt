package org.kodebase.common.job

object HeartBeatJob : RunnableJob<Unit> {

    override val schedule = every(minute = 1)

    override fun run() {
        println("Heart beat job executed")
    }

    override fun toString() = "${this::class.simpleName}(schedule=$schedule)"
}