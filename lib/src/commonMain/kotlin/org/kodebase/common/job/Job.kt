package org.kodebase.common.job

interface Job {

    val schedule: JobSchedule
}