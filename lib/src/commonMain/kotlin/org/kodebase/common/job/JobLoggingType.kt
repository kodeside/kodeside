package org.kodebase.common.job

enum class JobLoggingType { Memory, Mongo }