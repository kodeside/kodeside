package org.kodebase.common.job

import kotlinx.datetime.Instant
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

data class JobSchedule(
    val type: JobScheduleType,
    val month: Int = -1,
    val day: Int = -1,
    val dayOfWeek: Int = -1,
    val hour: Int = -1,
    val minute: Int = -1
) {
    fun shouldRunAt(time: Instant, timeZone: TimeZone = TimeZone.currentSystemDefault()) =
        time.toLocalDateTime(timeZone).let {
            when (type) {
                JobScheduleType.At ->
                    (month == -1 || month == it.monthNumber) &&
                    (day == -1 || day == it.dayOfMonth) &&
                    (dayOfWeek == -1 || dayOfWeek == it.dayOfWeek.value) &&
                    (hour == -1 || hour == it.hour) &&
                    (minute == -1 || minute == it.minute)

                JobScheduleType.Every ->
                    (month == -1 || it.monthNumber.mod(month) == 0) &&
                    (day == -1 || it.dayOfMonth.mod(day) == 0) &&
                    (dayOfWeek == -1 || it.dayOfWeek.value.mod(dayOfWeek) == 0) &&
                    (hour == -1 || it.hour.mod(hour) == 0) &&
                    (minute == -1 || it.minute.mod(minute) == 0)
            }
        }
}