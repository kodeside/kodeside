package org.kodebase.common.job

enum class JobScheduleType { At, Every }