package org.kodebase.common.job

interface RunnableJob<R : Any?> : Job {

    fun run(): R
}