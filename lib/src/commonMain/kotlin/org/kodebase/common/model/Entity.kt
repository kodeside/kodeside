package org.kodebase.common.model

import org.kodebase.common.type.ID

interface Entity {
    val _id: ID
}