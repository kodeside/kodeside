package org.kodebase.common.model

import org.kodebase.common.type.*
import kotlinx.datetime.Clock.System.now
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class JobLog(
    override val _id: ID = newId(),
    val name: String,
    val time: Instant = now(),
    var endTime: Instant? = null,
    var status: Status = Status.Running,
    var result: Value? = null,
    var output: String? = null
) : Entity {
    enum class Status { Running, Finished, Failed, TimedOut }
}