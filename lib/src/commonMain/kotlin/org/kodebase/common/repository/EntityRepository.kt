package org.kodebase.common.repository

import org.kodebase.common.model.Entity
import org.kodebase.common.type.Countable
import org.kodebase.common.type.ID

interface EntityRepository<E : Entity, F> : Countable {

    fun get(id: ID): E

    fun find(id: ID): E?

    fun find(filter: F): List<E>

    fun find(ids: List<ID>): List<E>

    fun save(entity: E): Boolean

    fun delete(id: ID): Boolean

    fun exists(id: ID): Boolean

    fun count(filter: F): Int
}
