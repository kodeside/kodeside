package org.kodebase.common.repository

import org.kodebase.common.model.JobLog

interface JobLogRepository<F> : EntityRepository<JobLog, F> {
    fun findBy(name: String, status: JobLog.Status): JobLog?
}