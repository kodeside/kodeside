package org.kodebase.common.repository

import org.kodebase.common.model.Entity
import org.kodebase.common.type.ID
import org.kodebase.util.LoggerDelegate

open class MemoryEntityRepository<E : Entity> : EntityRepository<E, (E) -> Boolean> {

    protected val entities = mutableMapOf<ID, E>()
    protected val logger by LoggerDelegate()

    val all: List<E> get() = entities.values.toList()

    override val count get() = entities.size

    override fun get(id: ID) = find(id) ?: error("Entity $id not found")

    override fun find(id: ID) = entities[id]

    override fun find(ids: List<ID>) = entities.values.filter { ids.contains(it._id) }

    override fun find(filter: (E) -> Boolean) = entities.values.filter(filter)

    override fun save(entity: E): Boolean {
        entities[entity._id] = entity
        logger.info("Entity $entity saved to memory")
        return true
    }

    override fun delete(id: ID) = entities.remove(id)?.let { true } ?: false

    override fun exists(id: ID) = entities.containsKey(id)

    override fun count(filter: (E) -> Boolean) = entities.values.count(filter)
}