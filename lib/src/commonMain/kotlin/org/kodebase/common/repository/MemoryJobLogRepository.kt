package org.kodebase.common.repository

import org.kodebase.common.model.JobLog

class MemoryJobLogRepository : JobLogRepository<(JobLog) -> Boolean>, MemoryEntityRepository<JobLog>() {

    override fun findBy(name: String, status: JobLog.Status) = all.firstOrNull { it.name == name && it.status == status }
    override fun count(filter: (JobLog) -> Boolean) = find(filter).size
}