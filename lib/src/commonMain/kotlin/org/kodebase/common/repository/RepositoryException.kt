package org.kodebase.common.repository

open class RepositoryException(
    override val message: String? = null,
    override val cause: Throwable? = null,
) : Throwable()
