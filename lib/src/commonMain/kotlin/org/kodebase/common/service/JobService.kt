package org.kodebase.common.service

import kotlinx.datetime.Clock.System.now
import org.kodebase.common.job.CommandJob
import org.kodebase.common.job.EventJob
import org.kodebase.common.job.Job
import org.kodebase.common.job.RunnableJob
import org.kodebase.common.model.JobLog
import org.kodebase.common.repository.JobLogRepository
import org.kodebase.common.type.Value
import org.kodebase.messaging.command.CommandBus
import org.kodebase.messaging.dispatch
import org.kodebase.messaging.event.EventBus
import org.kodebase.util.LoggerDelegate

class JobService(
    private val jobLogRepository: JobLogRepository<*>,
    private val commandBus: CommandBus,
    private val eventBus: EventBus,
    private val jobs: Set<Job>,
) {
    private val timeout: Int = 180000
    private val logger by LoggerDelegate()

    private fun run(job: Job, runAlways: Boolean): JobLog? {
        val name = job::class.qualifiedName ?: "unknown"
        var jobLog = jobLogRepository.findBy(name, JobLog.Status.Running)
        return if (jobLog != null && jobLog.status == JobLog.Status.Running) {
            val now = now()
            if (jobLog.time.toEpochMilliseconds() + timeout < now.toEpochMilliseconds()) {
                jobLog.status = JobLog.Status.TimedOut
                jobLogRepository.save(jobLog)
                logger.warn("Job ${jobLog.name} timed out")
            } else {
                logger.warn("Job ${jobLog.name} still running, cannot run again")
            }
            jobLog
        } else {
            jobLog = JobLog(name = name)
            if (runAlways || job.schedule.shouldRunAt(jobLog.time)) {
                try {
                    jobLogRepository.save(jobLog)
                    logger.info(jobLog.toString())
                    val result = when (job) {
                        is RunnableJob<*> -> job.run()
                        is CommandJob<*> -> job.command?.let {
                            commandBus.dispatch(it)
                        }
                        is EventJob -> job.event?.let {
                            eventBus.dispatch(it)
                        }
                        else -> null
                    }
                    jobLog.status = JobLog.Status.Finished
                    if (result != null && result != Unit)
                        jobLog.result = Value(result)
                } catch (e: Throwable) {
                    jobLog.status = JobLog.Status.Failed
                    jobLog.result = Value(e.message ?: "Unknown")
                } finally {
                    jobLogRepository.save(jobLog)
                    logger.info(jobLog.toString())
                }
                jobLog
            } else
                null
        }
    }

    fun run(name: String, runAlways: Boolean = true) = jobs.firstOrNull { it::class.qualifiedName == name }?.let {
        run(it, true)
    }

    fun runAll() = mutableListOf<JobLog>().apply {
        jobs.forEach { job ->
            run(job, false)?.let { jobLog ->
                add(jobLog)
            }
        }
    }
}