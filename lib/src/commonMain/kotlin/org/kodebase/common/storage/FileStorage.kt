package org.kodebase.common.storage

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.io.OutputStream

interface FileStorage {

    companion object {
        const val defaultContentType = "application/octet-stream"
    }

    fun read(path: String, output: OutputStream)
    fun get(path: String): StorageFile
    fun write(path: String, contentType: String, input: InputStream)
    fun write(path: String, contentType: String, bytes: ByteArray) = write(path, contentType, ByteArrayInputStream(bytes))
    fun delete(path: String): Boolean
}