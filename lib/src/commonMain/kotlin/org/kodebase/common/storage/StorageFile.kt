package org.kodebase.common.storage

import kotlinx.serialization.Serializable

@Serializable
data class StorageFile(
    var name: String,
    var size: Long,
    var contentType: String,
    var createTime: Long,
    var updateTime: Long,
    var metadata: Map<String, String>? = mapOf()
)