package org.kodebase.common.type

enum class CoordinateType(val limit: Int)  {
    Latitude(90), Longitude(180)
}