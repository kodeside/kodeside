package org.kodebase.common.type

interface Countable {

    val count: Int
}