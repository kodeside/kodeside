package org.kodebase.common.type

import kotlinx.serialization.Serializable

@Serializable
data class Counter(val name: String, var value: Int)