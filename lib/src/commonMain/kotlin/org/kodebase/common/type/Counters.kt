package org.kodebase.common.type

class Counters : ArrayList<Counter>() {

    fun increment(name: String) {
        val counter = firstOrNull { name == it.name }
        if (counter != null) {
            counter.value++
        } else {
            this += Counter(name, 1)
        }
    }
}