package org.kodebase.common.type

import kotlinx.serialization.Serializable

@JvmInline
@Serializable
value class Email(val email: String) {
    init {
        require(email.contains("@")) { "Invalid email" }
    }

    override fun toString() = email
}