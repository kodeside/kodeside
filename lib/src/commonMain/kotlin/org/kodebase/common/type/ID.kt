package org.kodebase.common.type

expect interface ID

expect fun newId(): ID

expect fun String.toID(): ID