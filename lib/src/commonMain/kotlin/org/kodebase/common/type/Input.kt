package org.kodebase.common.type

/**
 * Interface to mark GraphQL input class that have equivalent class in app/domain layer.
 */
interface Input<T> {
    /**
     * Note: it's not a property to avoid automatic serialization.
     */
    fun toObject(): T
}