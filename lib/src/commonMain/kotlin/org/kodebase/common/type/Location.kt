package org.kodebase.common.type

import kotlin.math.PI
import kotlin.math.acos
import kotlin.math.cos
import kotlin.math.sin

interface Location {
    val latitude: Double
    val longitude: Double

    operator fun minus(l: Location) =
        (((acos(
            sin((l.latitude * PI / 180.0)) * sin((latitude * PI / 180.0)) + cos((l.latitude * PI / 180.0)) * cos((latitude * PI / 180.0)) * cos(
                ((l.longitude - longitude) * PI / 180.0)
            )
        ))*180.0/ PI)*60*1.1515*1.609344)

}