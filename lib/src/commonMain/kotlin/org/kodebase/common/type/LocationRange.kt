package org.kodebase.common.type

data class LocationRange(val min: Location, val max: Location)