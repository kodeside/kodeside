package org.kodebase.common.type

import kotlinx.serialization.Serializable

@JvmInline
@Serializable
value class PhoneNumber(val phoneNumber: String) {
    init {
        require(phoneNumber.startsWith('+')) { "Invalid phone number" }
    }

    override fun toString() = phoneNumber

    companion object {
        val empty = PhoneNumber("+0")
    }
}