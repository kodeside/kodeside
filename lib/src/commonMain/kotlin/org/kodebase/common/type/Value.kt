package org.kodebase.common.type

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.SerializationException
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

@Serializable(with = Value.Serializer::class)
data class Value(val value: Any) {

    object Serializer : KSerializer<Value> {

        override val descriptor = buildClassSerialDescriptor("Value") {
            element("type", serialDescriptor<String>())
            element("value", buildClassSerialDescriptor("Any"))
        }

        @OptIn(ExperimentalSerializationApi::class)
        override fun deserialize(decoder: Decoder) = decoder.decodeStructure(descriptor) {
            if (decodeSequentially()) {
                val type = decodeStringElement(descriptor, 0)
                val value = decodeSerializableElement(descriptor, 1, serializer(type))
                Value(value)
            } else {
                require(decodeElementIndex(descriptor) == 0) { "type field should precede value field" }
                val type = decodeStringElement(descriptor, 0)
                val value = when (val index = decodeElementIndex(descriptor)) {
                    1 -> decodeSerializableElement(descriptor, 1, serializer(type))
                    CompositeDecoder.DECODE_DONE -> throw SerializationException("value field is missing")
                    else -> error("Unexpected index: $index")
                }
                Value(value)
            }
        }

        override fun serialize(encoder: Encoder, value: Value) = encoder.encodeStructure(descriptor) {
            val type = value.value::class.qualifiedName!!
            encodeStringElement(descriptor, 0, type)
            encodeSerializableElement(descriptor, 1, serializer(type), value.value)
        }

        private fun serializer(type: String): KSerializer<Any> = when(type) {
            Boolean::class.qualifiedName -> Boolean.serializer()
            String::class.qualifiedName -> String.serializer()
            Short::class.qualifiedName -> Short.serializer()
            Byte::class.qualifiedName -> Byte.serializer()
            Int::class.qualifiedName -> Int.serializer()
            Long::class.qualifiedName -> Long.serializer()
            Float::class.qualifiedName -> Float.serializer()
            Double::class.qualifiedName -> Double.serializer()
            else -> error("Value type $type is not supported for serialization")
        } as KSerializer<Any>
    }
}