package org.kodebase.config

expect class Config {
    operator fun get(name: String): String

    fun getOrNull(name: String): String?

    fun getOrDefault(name: String, default: String): String
}

inline fun <reified T> Config.getTyped(key: String): T = with(this[key]) {
    when (T::class) {
        String::class -> this
        Short::class -> toShort()
        Byte::class -> toByte()
        Int::class -> toInt()
        Long::class -> toLong()
        Float::class -> toFloat()
        Double::class -> toDouble()
        Boolean::class -> toBoolean()
        else -> error("Unsupported config type: ${T::class.simpleName}")
    } as T
}
