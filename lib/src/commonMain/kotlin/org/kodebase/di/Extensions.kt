package org.kodebase.di

import org.kodebase.config.Config
import org.kodebase.config.getTyped
import org.kodein.di.DirectDIAware
import org.kodein.di.instance

inline fun <reified T> DirectDIAware.config(key: String): T = instance<Config>().getTyped(key)
