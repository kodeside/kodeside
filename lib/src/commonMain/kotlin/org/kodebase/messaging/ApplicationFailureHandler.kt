package org.kodebase.messaging

import org.kodebase.common.exception.ApplicationException

class ApplicationFailureHandler<M : Message<R>, R> : FailureHandler<M, R, ApplicationException> {
    override fun handle(envelope: Envelope<M, R>, exception: Throwable): ApplicationException {
        return if (exception is ApplicationException) {
            if (exception.language == null)
                ApplicationException(exception.message, envelope.language, exception.cause, *exception.arguments)
            else
                exception
        } else {
            ApplicationException(exception.message ?: "Exception.Unknown", envelope.language, exception.cause)
        }.also {
            exception.printStackTrace()
        }
    }
}
