package org.kodebase.messaging

class BaseEnvelope<out M : Message<R>, out R>(
    override val message: M,
) : Envelope<M, R> {

    override var stamps = arrayOf<Stamp>()
        private set

    override fun stamp(stamp: Stamp) {
        stamps = (stamps.filter { it::class != stamp::class } + stamp).toTypedArray()
    }

    override fun toString() = "${this::class.simpleName}(message=$message, stamps=${stamps.toList()})"
}
