package org.kodebase.messaging

import kotlin.reflect.KClass

interface Envelope<out M : Message<R>, out R> {
    val message: M
    val stamps: Array<Stamp>

    fun stamp(stamp: Stamp)

    fun hasStamp(clazz: KClass<*>): Boolean = stamps.any { it::class == clazz }

    @Suppress("UNCHECKED_CAST")
    fun <S : Stamp> getStamp(clazz: KClass<S>): S = (stamps.singleOrNull { it::class == clazz }?: error("Missing stamp of type ${clazz.simpleName}")) as S
}

inline fun <reified S> Envelope<*, *>.hasStamp() = hasStamp(S::class)
inline fun <reified S:Stamp> Envelope<*, *>.getStamp(): S = getStamp(S::class)

