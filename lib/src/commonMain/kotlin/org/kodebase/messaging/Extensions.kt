package org.kodebase.messaging

import kotlinx.serialization.encodeToString
import kotlinx.serialization.serializer
import org.kodebase.common.exception.ApplicationException
import org.kodebase.common.type.Value
import org.kodebase.messaging.command.Command
import org.kodebase.messaging.event.Event
import org.kodebase.messaging.query.Query
import org.kodebase.security.Identity
import org.kodebase.security.UserPrincipal
import org.kodebase.security.exception.UnauthorizedException
import kotlin.reflect.typeOf

val Identity.stamp get() = IdentityStamp(this)

val Envelope<*, *>.identity get() = getStamp(IdentityStamp::class).identity

val Envelope<*, *>.userPrincipalOrNull: UserPrincipal?
    get() {
        return if (hasStamp<UserPrincipalStamp>())
            getStamp<UserPrincipalStamp>().userPrincipal
        else
            null
    }

val Envelope<*, *>.userPrincipal: UserPrincipal
    get() = userPrincipalOrNull ?: throw UnauthorizedException("Envelope for $message has no UserPrincipalStamp set")


val Envelope<*, *>.language: String
    get() = userPrincipalOrNull?.language ?: "en" //TODO LanguageStamp

val <Q: Query<R>, R>Envelope<Q, R>.query get() = this.message

val <C: Command<R>, R>Envelope<C, R>.command get() = this.message

val <E: Event>Envelope<E, Unit>.event get() = this.message

inline val Message<*>.typeName get() = when(this) {
    is Query -> "Query"
    is Command -> "Command"
    is Event -> "Event"
    else -> this::class.simpleName!!
}

inline fun <reified MM : Message<RR>, reified RR> MessageBus<*, *>.dispatch(message: MM, vararg stamp: Stamp): RR =
    dispatch(message, typeOf<RR>(), *stamp)

inline fun <reified R> result(block: () -> R): MessageHandlerResult<R> =
    try {
        val value = block.invoke()
        MessageHandlerResult(Result.success(value)) { stringFormat ->
            stringFormat.encodeToString(value)
        }
    } catch (t: Throwable) {
        MessageHandlerResult(Result.failure(t)) { stringFormat ->
            val serializer = stringFormat.serializersModule.serializer<MessageHandlerFailure>()
            stringFormat.encodeToString(serializer,
                if (t is ApplicationException)
                    MessageHandlerFailure(t.message, t.language, t.arguments.map { Value(it) })
                else
                    MessageHandlerFailure(t.message)
            )
        }
    }