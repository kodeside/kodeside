package org.kodebase.messaging

fun interface FailureHandler<M : Message<R>, R, E : Throwable> {

    fun handle(envelope: Envelope<M, R>, exception: Throwable): E
}
