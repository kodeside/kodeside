package org.kodebase.messaging

import org.kodebase.security.Identity
import kotlinx.serialization.Serializable

@Serializable
data class IdentityStamp(
    val identity: Identity,
) : Stamp