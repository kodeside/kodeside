package org.kodebase.messaging

import kotlin.reflect.KType

interface MessageBus<M : Message<R>, R> {
    fun <MM : Message<RR>, RR> dispatch(message: Message<RR>, resultType: KType, vararg stamp: Stamp): RR
}
