package org.kodebase.messaging

interface MessageBusMiddleware {
    fun process(envelope: Envelope<Message<*>, *>): Envelope<*, *>
}
