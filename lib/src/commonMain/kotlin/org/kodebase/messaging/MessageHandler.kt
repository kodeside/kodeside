package org.kodebase.messaging

import kotlin.reflect.KClass

interface MessageHandler<in M : Message<R>, R> {

    val canHandle: KClass<in M>

    fun handle(envelope: Envelope<M, R>): MessageHandlerResult<R>
}
