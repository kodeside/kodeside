package org.kodebase.messaging

import kotlinx.serialization.Serializable
import org.kodebase.common.type.Value

@Serializable
data class MessageHandlerFailure(
    val message: String?,
    val language: String? = null,
    val arguments: List<Value> = emptyList()
)