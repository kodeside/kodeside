package org.kodebase.messaging

import kotlinx.serialization.StringFormat

class MessageHandlerResult<T>(val result: Result<T>, val encoder: Encoder) {

    fun interface Encoder {
        fun encodeToString(stringFormat: StringFormat): String
    }

    val isSuccess: Boolean get() = result.isSuccess

    val isFailure: Boolean get() = result.isFailure

    inline fun getOrNull() = result.getOrNull()

    fun exceptionOrNull() = result.exceptionOrNull()

    fun getOrThrow() = result.getOrThrow()
}