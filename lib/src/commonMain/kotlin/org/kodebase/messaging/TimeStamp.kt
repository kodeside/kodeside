package org.kodebase.messaging

import kotlinx.datetime.Clock.System.now
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class TimeStamp(
    val time: Instant = now(),
) : Stamp
