package org.kodebase.messaging

import org.kodebase.security.UserPrincipal

data class UserPrincipalStamp(
    val userPrincipal: UserPrincipal,
) : Stamp