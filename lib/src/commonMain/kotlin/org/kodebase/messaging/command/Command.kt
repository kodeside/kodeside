package org.kodebase.messaging.command

import org.kodebase.messaging.Message

interface Command<R : Any> : Message<R>