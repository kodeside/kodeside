package org.kodebase.messaging.command

import org.kodebase.messaging.MessageBus

interface CommandBus : MessageBus<Command<Any>, Any>
