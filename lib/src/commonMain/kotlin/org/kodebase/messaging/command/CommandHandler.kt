package org.kodebase.messaging.command

import org.kodebase.messaging.MessageHandler

interface CommandHandler<in C : Command<R>, R : Any> : MessageHandler<C, R>
