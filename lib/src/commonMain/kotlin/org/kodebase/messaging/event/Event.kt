package org.kodebase.messaging.event

import org.kodebase.messaging.Message

// Mark up interface for events
interface Event : Message<Unit>