package org.kodebase.messaging.event

import org.kodebase.messaging.MessageBus
import org.kodebase.messaging.Stamp
import kotlin.reflect.typeOf

interface EventBus : MessageBus<Event, Unit> {
    fun dispatch(source: EventSource, vararg stamp: Stamp) {
        source.events.forEach { dispatch(it, typeOf<Unit>(), *stamp) }
        source.clearEvents()
    }
}