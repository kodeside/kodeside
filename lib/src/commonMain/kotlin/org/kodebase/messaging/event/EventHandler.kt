package org.kodebase.messaging.event

import org.kodebase.messaging.MessageHandler

interface EventHandler<in E : Event> : MessageHandler<E, Unit>