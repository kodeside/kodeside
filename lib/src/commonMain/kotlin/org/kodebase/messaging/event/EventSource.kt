package org.kodebase.messaging.event

interface EventSource {
    val events: List<Event>
    fun clearEvents()
}