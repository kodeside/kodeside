package org.kodebase.messaging.provider

import org.kodebase.messaging.*

abstract class AbstractMessageBus<M : Message<R>, R, E : Throwable> : MessageBus<M, R> {

    abstract val middlewares: Set<MessageBusMiddleware>
    abstract val failureHandler: FailureHandler<M, R, E>

    protected fun wrap(message: M, stamps: Array<out Stamp>): Envelope<M, R> =
        BaseEnvelope(message).apply {
            this.stamp(TimeStamp())
            stamps.forEach { this.stamp(it) }
        }

    protected fun fold(message: M, stamps: Array<out Stamp>) =
        middlewares.fold(wrap(message, stamps)) { e, it ->
            it.process(e) as? Envelope<M, R> ?: error("Bus middleware cast error")
        }
}