package org.kodebase.messaging.query

import org.kodebase.messaging.Message

interface Query<out R : Any?> : Message<R>