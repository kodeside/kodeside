package org.kodebase.messaging.query

import org.kodebase.messaging.MessageBus

interface QueryBus : MessageBus<Query<Any>, Any>