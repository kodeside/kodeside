package org.kodebase.messaging.query

import org.kodebase.messaging.Envelope
import org.kodebase.messaging.MessageHandler
import org.kodebase.messaging.MessageHandlerResult

interface QueryHandler<in Q : Query<R>, R> : MessageHandler<Q, R> {

//    @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
//    override fun handle(query: Q, identity: Identity): Result<R>

    override fun handle(envelope: Envelope<Q, R>): MessageHandlerResult<R>
}