package org.kodebase.monitoring

import org.kodebase.util.LoggerDelegate

abstract class AbstractMonitoring : Monitoring {

    val logger by LoggerDelegate()

    init {
        logger.info("Monitoring created")
    }
}