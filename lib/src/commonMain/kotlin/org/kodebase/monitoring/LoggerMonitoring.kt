package org.kodebase.monitoring

import org.kodebase.monitoring.performance.LoggerTransaction
import org.kodebase.monitoring.performance.PerformanceTransaction
import org.kodebase.security.UserPrincipal

object LoggerMonitoring : AbstractMonitoring() {

    private var persistentTransactions: MutableMap<Pair<String, String>, PerformanceTransaction> = mutableMapOf()

    override fun capture(text: String?, error: Throwable?, tags: Map<String, String>) {
        println("Monitoring event: ${text?:error?.message?:"Unknown"}")
        tags.forEach {
            println("${it.key} = ${it.value}")
        }
    }

    override fun message(text: String?) {
        println(text)
    }

    override fun createPerformanceTransaction(
        name: String,
        operation: String,
        userPrincipal: UserPrincipal?,
        tags: MutableMap<String, String>,
        persistent: Boolean
    ): PerformanceTransaction {
        return if (persistent) {
            persistentTransactions[Pair(name, operation)] = LoggerTransaction(name, operation, userPrincipal, tags)
            getPerformanceTransaction(name, operation)!!
        } else {
            LoggerTransaction(name, operation, userPrincipal, tags)
        }
    }

    override fun getPerformanceTransaction(name: String, operation: String): PerformanceTransaction? {
        return persistentTransactions[Pair(name, operation)]
    }

    override fun removePerformanceTransaction(name: String, operation: String) {
        persistentTransactions.remove(Pair(name, operation))
    }

    override fun setForceLogging(toValue: Boolean) {}
}