package org.kodebase.monitoring

import org.kodebase.monitoring.performance.PerformanceTransaction
import org.kodebase.security.UserPrincipal

interface Monitoring {

    fun capture(
        text: String?,
        error: Throwable?,
        tags: Map<String, String> = mapOf()
    )

    fun message(
        text: String?,
    )

    fun createPerformanceTransaction(
        name: String,
        operation: String,
        userPrincipal: UserPrincipal? = null,
        tags: MutableMap<String, String> = mutableMapOf(),
        persistent: Boolean = false
    ) : PerformanceTransaction

    fun getPerformanceTransaction(name: String, operation: String): PerformanceTransaction?

    fun removePerformanceTransaction(name: String, operation: String)

    fun setForceLogging(toValue: Boolean)

    fun <T> performanceTransaction(
        name: String,
        operation: String,
        userPrincipal: UserPrincipal? = null,
        tags: MutableMap<String, String> = mutableMapOf(),
        persistent: Boolean = false,
        trace: String? = null,
        block: PerformanceTransaction.() -> T
    ) = with(createPerformanceTransaction(name, operation, userPrincipal, tags, persistent)) {
        measure(trace) { block() }
    }
}