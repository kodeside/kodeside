package org.kodebase.monitoring

import org.kodebase.monitoring.performance.BlankSpan
import org.kodebase.monitoring.performance.PerformanceTransaction
import org.kodebase.security.UserPrincipal

object NoMonitoring : AbstractMonitoring() {

    override fun capture(text: String?, error: Throwable?, tags: Map<String, String>) {}
    override fun message(text: String?) {}

    override fun createPerformanceTransaction(
        name: String,
        operation: String,
        userPrincipal: UserPrincipal?,
        tags: MutableMap<String, String>,
        persistent: Boolean
    ): PerformanceTransaction =
        object : PerformanceTransaction, BlankSpan(null, operation, tags) {
            override val name = name
            override val userPrincipal = userPrincipal
            override val trace: String? = null
            override fun start(trace: String?) {}
        }

    override fun getPerformanceTransaction(name: String, operation: String): PerformanceTransaction? = null
    override fun removePerformanceTransaction(name: String, operation: String) {}
    override fun setForceLogging(toValue: Boolean) {}
}