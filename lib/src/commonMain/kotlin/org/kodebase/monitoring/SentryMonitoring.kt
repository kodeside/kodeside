package org.kodebase.monitoring

expect class SentryMonitoring(
    dsn: String? = null,
    release: String? = null,
    environment: String? = null,
    tracesSampleRate: Double = 1.0,
    isTraceSampling: Boolean = true
)