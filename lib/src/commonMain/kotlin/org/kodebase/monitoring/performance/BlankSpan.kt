package org.kodebase.monitoring.performance

open class BlankSpan(
    override val parent: PerformanceSpan?,
    override val operation: String,
    override val tags: MutableMap<String, String> = mutableMapOf(),
    override val children: MutableList<PerformanceSpan> = mutableListOf()
) : PerformanceSpan {
    override fun createChild(operation: String, tags: MutableMap<String, String>) =
        BlankSpan(parent, operation, tags)
    override fun start() {}
    override fun abort() {}
    override fun abort(e: Throwable) {}
    override fun finish() {}
}