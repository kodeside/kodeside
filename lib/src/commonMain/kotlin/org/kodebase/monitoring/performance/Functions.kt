package org.kodebase.monitoring.performance

import kotlinx.datetime.Clock

fun currentTime() = Clock.System.now().toEpochMilliseconds()

fun <T> measure(callback: (Result<T>, Long) -> Unit, block: () -> T): T {
    val time = currentTime()
    return try {
        block().also {
            callback.invoke(Result.success(it), currentTime() - time)
        }
    } catch (e: Throwable) {
        callback.invoke(Result.failure(e), currentTime() - time)
        throw e
    }
}

