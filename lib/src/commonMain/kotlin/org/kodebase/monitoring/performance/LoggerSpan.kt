package org.kodebase.monitoring.performance

import org.kodebase.util.LoggerDelegate

open class LoggerSpan(
    override val parent: LoggerSpan?,
    override val operation: String,
    override val tags: MutableMap<String, String>,
    override val children: MutableList<PerformanceSpan> = mutableListOf()
) : PerformanceSpan {

    protected val logger by LoggerDelegate()

    override fun createChild(operation: String, tags: MutableMap<String, String>) =
        LoggerSpan(this, operation, tags)

    override fun start() = logger.info("[$qualifiedName] started with tags $tags")

    override fun abort() = logger.info("[$qualifiedName] aborted")

    override fun abort(e: Throwable) = logger.error("[$qualifiedName] aborted: ${e.message}")

    override fun finish() = logger.info("[$qualifiedName] finished")
}