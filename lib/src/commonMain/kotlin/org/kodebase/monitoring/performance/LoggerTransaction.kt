package org.kodebase.monitoring.performance

import org.kodebase.security.UserPrincipal

class LoggerTransaction(
    override val name: String,
    operation: String,
    override val userPrincipal: UserPrincipal?,
    tags: MutableMap<String, String>
) : PerformanceTransaction, LoggerSpan(null, operation, tags) {

    override val trace: String? = null

    override fun start(trace: String?) {
        logger.info("[$this] started for ${userPrincipal ?: "<anonymous>"} with tags $tags")
    }

    override fun toString() = "${this::class.simpleName}(name=$name, operation=$operation, userPrincipal=$userPrincipal, tags=$tags)"
}