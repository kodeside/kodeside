package org.kodebase.monitoring.performance

interface  PerformanceSpan {
    val parent: PerformanceSpan?
    val operation: String
    val tags: MutableMap<String, String>
    val qualifiedName get() = "${if (this is PerformanceTransaction) "$name:" else ""}${parent?.let { "$it/" } ?:""}$operation"
    val children: MutableList<PerformanceSpan>

    fun createChild(operation: String, tags: MutableMap<String, String> = mutableMapOf()): PerformanceSpan
    fun start()
    fun abort()
    fun abort(e: Throwable)
    fun finish()
    fun mark(operation: String, tags: MutableMap<String, String> = mutableMapOf()) =
        with (createChild(operation, tags)) {
            start()
            finish()
        }

    fun <T> run(callback: ((Result<T>, Long) -> Unit)? = null, block: () -> T): T {
        val time = currentTime()
        return try {
            block().also {
                callback?.invoke(Result.success(it), currentTime() - time)
            }
        } catch (e: Throwable) {
            abort(e)
            callback?.invoke(Result.failure(e), currentTime() - time)
            throw e
        } finally {
            finish()
        }
    }

    fun <T> measure(callback: ((Result<T>, Long) -> Unit)? = null, block: () -> T): T {
        start()
        return run(callback, block)
    }

    fun <T> performanceSpan(
        operation: String,
        tags: MutableMap<String, String> = mutableMapOf(),
        block: PerformanceSpan.() -> T
    ) = with(createChild(operation, tags)) {
        measure { block() }
    }
}