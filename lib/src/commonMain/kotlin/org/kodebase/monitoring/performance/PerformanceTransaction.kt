package org.kodebase.monitoring.performance

import org.kodebase.security.UserPrincipal

interface PerformanceTransaction : PerformanceSpan {
    val name: String
    val userPrincipal: UserPrincipal?
    val trace: String?
    fun start(trace: String?)

    fun <T> measure(trace: String? = null, callback: ((Result<T>, Long) -> Unit)? = null, block: () -> T): T {
        start(trace)
        return run(callback, block)
    }


}