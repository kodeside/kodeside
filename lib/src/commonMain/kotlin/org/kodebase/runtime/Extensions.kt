package org.kodebase.runtime

expect fun getenv(name: String): String?