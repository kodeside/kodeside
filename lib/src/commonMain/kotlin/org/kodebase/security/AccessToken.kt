package org.kodebase.security

import kotlinx.serialization.Serializable

@JvmInline
@Serializable
value class AccessToken(val token: String)/* : Stamp*/ {
    init {
        require(token.isNotBlank())
    }
}