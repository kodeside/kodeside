package org.kodebase.security

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Nobody")
object Nobody : Identity