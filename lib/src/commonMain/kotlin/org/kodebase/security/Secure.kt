@file:OptIn(ExperimentalContracts::class)

package org.kodebase.security

import org.kodebase.security.exception.SecurityException
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract


fun secure(value: Boolean): Unit {
    contract {
        returns() implies value
    }
    secure(value) { "Security check failed." }
}

inline fun secure(value: Boolean, lazyMessage: () -> Any): Unit {
    contract {
        returns() implies value
    }
    if (!value) {
        val message = lazyMessage()
        throw SecurityException(message.toString())
    }
}
