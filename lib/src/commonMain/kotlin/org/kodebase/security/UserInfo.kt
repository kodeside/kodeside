package org.kodebase.security

import org.kodebase.common.type.Email
import kotlinx.datetime.LocalDate
import kotlinx.serialization.Serializable

@Serializable
data class UserInfo(
    val email: Email,
    val email_verified: Boolean,
    val sub: String,
    val given_name: String? = null,
    val family_name: String? = null,
    val nickname: String? = null,
    val name: String? = null,
    val picture: String? = null,
    val locale: String? = null,
    val updated_at: String? = null,
    val birthday: String? = null
) {
    val dobAsLocalDate: LocalDate?
        get() = birthday?.let {
            val (month, day, year) = it.split("/").map { it.toInt() }
            LocalDate(year, month, day)
        }
}