package org.kodebase.security

import org.kodebase.common.type.ID
import kotlinx.serialization.Serializable

@Serializable
data class UserPrincipal(
    val id: ID,
    val name: String,
    val nickname: String?,
    val language: String
) {
    val ref get() = nickname ?: id.toString()
}