package org.kodebase.security.exception

class ForbiddenException(override val message: String) : SecurityException(message)