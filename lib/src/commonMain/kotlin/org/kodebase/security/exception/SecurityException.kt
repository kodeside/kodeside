package org.kodebase.security.exception

import org.kodebase.common.exception.DomainException

open class SecurityException(override val message: String?) : DomainException(message)