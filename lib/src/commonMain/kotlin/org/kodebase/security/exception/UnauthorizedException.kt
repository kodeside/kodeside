package org.kodebase.security.exception

class UnauthorizedException(override val message: String) : SecurityException(message)