package org.kodebase.util

interface Cache<T> {

    interface Item<T> {
        val value: T
        val isExpired: Boolean
    }
    operator fun set(key: Any, value: T)
    operator fun get(key: Any): T?
    fun remove(key: Any): T?
    fun clear()
    val size: Int
    val items: List<Item<T>>
    val values: List<T>
}