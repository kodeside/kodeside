package org.kodebase.util

fun String.removeWhitespaces() = replace("\\s".toRegex(), "")
