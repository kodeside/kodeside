package org.kodebase.util

import kotlin.random.Random

object Generator {

    fun randomCode(length: Int = 5, charPool: List<Char> = ('a'..'z') + ('0'..'9')) =
        (1..length)
            .map { Random.nextInt(0, charPool.size) }
            .map(charPool::get)
            .joinToString("")
}