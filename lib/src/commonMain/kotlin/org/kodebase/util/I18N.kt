package org.kodebase.util

expect class I18N {
    fun translate(text: String, language: String): String
    fun format(text: String, language: String, vararg args: Any?): String
}