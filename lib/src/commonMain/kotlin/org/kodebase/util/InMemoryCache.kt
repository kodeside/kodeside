package org.kodebase.util

import kotlinx.datetime.Clock

class InMemoryCache<T>(val maxSize: Int, val expirationInMillis: Long) : Cache<T> {

    inner class Item<T>(value: T) : Cache.Item<T> {

        var timestamp: Long = Clock.System.now().toEpochMilliseconds()
        var count: Int = 0

        override val value: T = value
            get() {
                timestamp = Clock.System.now().toEpochMilliseconds()
                count++
                return field
            }

        override val isExpired get() = Clock.System.now().toEpochMilliseconds() - timestamp > expirationInMillis

        override fun toString(): String {
            val time = Clock.System.now().toEpochMilliseconds() - timestamp
            return "${this::class.simpleName}($value, ${if (time > expirationInMillis) "EXPIRED" else "$time ms old"})"
        }
    }

    private val comparator =
        Comparator<Map.Entry<Any, Item<T>>> { e1, e2 -> (e1.value.timestamp - e2.value.timestamp).toInt() }

    private val map = mutableMapOf<Any, Item<T>>()

    override operator fun set(key: Any, value: T) {
        if (size == maxSize) {
            map.minWithOrNull(comparator)?.let {
                map.remove(it.key)
            }
        }
        map[key] = Item(value)
    }

    override operator fun get(key: Any) = map[key]?.let { if (it.isExpired) null else it.value }

    override fun remove(key: Any) = map.remove(key)?.value

    override fun clear() = map.clear()

    override val size get() = map.size

    override val items get() = map.values.toList()

    override val values get() = items.filter { !it.isExpired }.map { it.value }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val cache = InMemoryCache<String>(3, 500)
            cache[1] = "a"
            Thread.sleep(1000)
            cache[2] = "b"
            Thread.sleep(1000)
            cache[3] = "c"
            cache[4] = "d"
            cache[2]
            println(cache.items)
        }
    }
}