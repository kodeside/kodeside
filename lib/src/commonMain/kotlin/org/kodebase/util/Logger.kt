package org.kodebase.util

expect interface Logger {

    fun info(msg: String)
    fun warn(msg: String)
    fun debug(msg: String)
    fun error(msg: String)
    fun error(msg: String, t: Throwable)
}