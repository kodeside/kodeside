package org.kodebase.util

import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

expect class LoggerDelegate() : ReadOnlyProperty<Any?, Logger> {

    override operator fun getValue(thisRef: Any?, property: KProperty<*>): Logger
}