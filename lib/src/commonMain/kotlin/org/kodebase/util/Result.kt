package org.kodebase.util

fun <T> result(block: () -> T): Result<T> = try {
    Result.success(block.invoke())
} catch (t: Throwable) {
    Result.failure(t)
}
