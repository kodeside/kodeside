package com.apurebase.kgraphql

import com.apurebase.kgraphql.schema.dsl.SchemaBuilder
import com.apurebase.kgraphql.schema.dsl.SchemaConfigurationDSL
import io.ktor.server.application.*
import io.ktor.http.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.util.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.addJsonObject
import kotlinx.serialization.json.buildJsonArray
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.Json.Default.decodeFromString
import kotlinx.serialization.json.Json.Default.encodeToString
import kotlinx.serialization.json.put
import org.kodebase.common.exception.ApplicationException
import org.kodebase.util.I18N
import org.kodebase.util.LoggerDelegate

class GraphQLLazy {

    class Configuration: SchemaConfigurationDSL() {
        fun schema(block: SchemaBuilder.() -> Unit) {
            schemaBlock = block
        }

        /**
         * This adds support for opening the graphql route within the browser
         */
        var playground: Boolean = false

        var endpoint: String = "/graphql"

        fun context(block: ContextBuilder.(ApplicationCall) -> Unit) {
            contextSetup = block
        }

        fun wrap(block: Route.(next: Route.() -> Unit) -> Unit) {
            wrapWith = block
        }

        internal var contextSetup: (ContextBuilder.(ApplicationCall) -> Unit)? = null
        internal var wrapWith: (Route.(next: Route.() -> Unit) -> Unit)? = null
        internal var schemaBlock: (SchemaBuilder.() -> Unit)? = null

    }

    companion object Feature : BaseApplicationPlugin<Application, Configuration, GraphQLLazy> {

        var i18n: I18N = I18N(emptyMap())

        private val logger by LoggerDelegate()

        private val schemaBuilders = mutableListOf<SchemaBuilder.()->Unit>()

        fun addBuilder(block: SchemaBuilder.()->Unit) {
            schemaBuilders.add(block)
        }

        override val key = AttributeKey<GraphQLLazy>("KGraphQL")

        override fun install(pipeline: Application, configure: Configuration.() -> Unit): GraphQLLazy {
            val config = Configuration().apply(configure)
            val schema by lazy {
                KGraphQL.schema {
                    configuration = config
                    schemaBuilders.forEach { it.invoke(this) }
                }
            }

            val routing: Routing.() -> Unit = {
                val routing: Route.() -> Unit = {
                    route(config.endpoint) {
                        post {
                            val text = withContext(Dispatchers.IO) {
                                call.receiveStream().bufferedReader(Charsets.UTF_8).readText()
                            }
                            val request = decodeFromString(GraphqlRequest.serializer(), text)
                            when {
                                logger.isDebugEnabled -> logger.debug("[GRAPHQL REQUEST] ${request.operationName}\n ${encodeToString(
                                    GraphqlRequest.serializer(), request).replace("\\n", "\n")}")
                                logger.isInfoEnabled -> logger.info("[GRAPHQL REQUEST]) ${request.operationName}")
                            }
                            val ctx = context {
                                config.contextSetup?.invoke(this, call)
                            }
                            val result = schema.execute(request.query, request.variables.toString(), ctx)
                            call.respondText(result, contentType = ContentType.Application.Json)
                        }
                        if (config.playground) get {
                            @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
                            val playgroundHtml = KtorGraphQLConfiguration::class.java.classLoader.getResource("playground.html").readBytes()
                            call.respondBytes(playgroundHtml, contentType = ContentType.Text.Html)
                        }
                    }
                }

                config.wrapWith?.invoke(this, routing) ?: routing(this)
            }

            pipeline.pluginOrNull(Routing)?.apply(routing) ?: pipeline.install(Routing, routing)

            pipeline.intercept(ApplicationCallPipeline.Monitoring) {
                try {
                    coroutineScope {
                        proceed()
                    }
                } catch (e: Throwable) {
                    if (e is GraphQLError) {
                        logger.error(
                            (e.cause as? ApplicationException)?.run {
                                Feature.i18n.format(message, "en", *arguments)
                            } ?: e.cause?.message ?: e.message,
                            e.cause ?: e
                        )
                        context.respond(HttpStatusCode.OK, e.serialize(Feature.i18n))
                    } else throw e
                }
            }
            return GraphQLLazy()
        }

        private fun GraphQLError.serialize(i18n: I18N): String = buildJsonObject {
            put("errors", buildJsonArray {
                addJsonObject {
                    put("message", (cause as? ApplicationException)?.run {
                        i18n.format(message, language ?: "en", *arguments)
                    } ?: message)
                    put("locations", buildJsonArray {
                        locations?.forEach {
                            addJsonObject {
                                put("line", it.line)
                                put("column", it.column)
                            }
                        }
                    })
                    put("path", buildJsonArray {
                        // TODO: Build this path. https://spec.graphql.org/June2018/#example-90475
                    })
                }
            })
        }.toString()
    }

}