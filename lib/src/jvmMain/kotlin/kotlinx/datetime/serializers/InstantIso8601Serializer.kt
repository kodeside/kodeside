package kotlinx.datetime.serializers

import com.github.jershell.kbson.BsonEncoder
import com.github.jershell.kbson.FlexibleDecoder
import kotlinx.datetime.Instant
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.JsonEncoder
import org.bson.BsonType

public object InstantIso8601Serializer: KSerializer<Instant> {

    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("Instant", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): Instant {
        return when (decoder) {
            is JsonDecoder -> {
                Instant.parse(decoder.decodeString())
            }
            is FlexibleDecoder -> {
                when (decoder.reader.currentBsonType) {
                    BsonType.DATE_TIME -> Instant.fromEpochMilliseconds(decoder.reader.readDateTime())
                    BsonType.STRING ->  Instant.parse(decoder.decodeString())  //for backward compatibility
                    else -> error("BsonType not supported ${decoder.reader.currentBsonType}")
                }
            }
            else -> error("Decoder not supported")
        }
    }

    override fun serialize(encoder: Encoder, value: Instant) {
        when(encoder) {
            is JsonEncoder -> encoder.encodeString(value.toString())
            is BsonEncoder ->  encoder.encodeDateTime(value.toEpochMilliseconds())

        }
    }
}
