package kotlinx.datetime.serializers

import com.github.jershell.kbson.BsonEncoder
import com.github.jershell.kbson.FlexibleDecoder
import kotlinx.datetime.*
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.JsonEncoder
import org.bson.BsonType

public object LocalDateIso8601Serializer : KSerializer<LocalDate> {

    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("LocalDate", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): LocalDate {
        return when (decoder) {
            is JsonDecoder -> {
                Instant.parse(decoder.decodeString())
            }
            is FlexibleDecoder -> {
                when (decoder.reader.currentBsonType) {
                    BsonType.DATE_TIME -> Instant.fromEpochMilliseconds(decoder.reader.readDateTime())
                    BsonType.STRING -> {
                        decoder.decodeString().let {
                            if (it.matches("^\\d{4}-\\d{2}-\\d{2}\$".toRegex())) {
                                LocalDate.parse(it).atStartOfDayIn(TimeZone.UTC)
                            } else {
                                Instant.parse(decoder.decodeString())
                            }
                        }
                    }
                    else -> error("BsonType not supported ${decoder.reader.currentBsonType}")
                }
            }
            else -> error("Decoder not supported")
        }.toLocalDateTime(TimeZone.UTC).date
    }

    override fun serialize(encoder: Encoder, value: LocalDate) {
        when (encoder) {
            is JsonEncoder -> encoder.encodeString(value.atStartOfDayIn(TimeZone.UTC).toString())
            is BsonEncoder -> encoder.encodeDateTime(value.atStartOfDayIn(TimeZone.UTC).toEpochMilliseconds())

        }
    }
}
