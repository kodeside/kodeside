package org.kodebase.application

import org.kodebase.application.security.PrincipalIdentity
import org.kodebase.common.type.WrappedObjectId
import org.kodebase.security.Identity
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.http.*
import io.ktor.server.request.*
import java.util.*
import kotlin.reflect.full.isSubclassOf

inline val ApplicationCall.identity: Identity get() = principal<PrincipalIdentity>() ?: error("Missing identity in HTTP call")

inline val ApplicationCall.locale: Locale
    get() = with (request.acceptLanguageItems() as MutableList<HeaderValue>) {
    sortBy { it.params.firstOrNull { param -> param.name == "q" }?.value?.toFloat() ?: 1.0F }
    Locale.forLanguageTag(lastOrNull()?.value ?: "en")
}

inline fun <reified T : Number> ApplicationCall.numberParameter(name: String, defaultValue: T? = null): T =
    (parameters[name]?.let {
        if (Int::class.isSubclassOf(T::class))
            it.toInt()
        else if (Long::class.isSubclassOf(T::class))
            it.toLong()
        else if (Float::class.isSubclassOf(T::class))
            it.toFloat()
        else if (Double::class.isSubclassOf(T::class))
            it.toDouble()
        else if (Short::class.isSubclassOf(T::class))
            it.toShort()
        else if (Byte::class.isSubclassOf(T::class))
            it.toByte()
        else
            error("Unsupported number parameter type: ${T::class.simpleName}")
    } ?: defaultValue ?: error("Missing number parameter '$name'")) as T

fun ApplicationCall.booleanParameter(name: String, defaultValue: Boolean? = null) =
    parameters[name]?.toBoolean() ?: defaultValue ?: error("Missing boolean parameter '$name'")

fun ApplicationCall.stringParameter(name: String, defaultValue: String? = null) =
    parameters[name] ?: defaultValue ?: error("Missing string parameter '$name'")

fun ApplicationCall.idParameter(name: String) = parameters[name]?.let { WrappedObjectId(it) } ?: error("Missing id parameter '$name'")

fun ApplicationCall.idParameterOrNull(name: String) = parameters[name]?.let { WrappedObjectId(it) }

fun ApplicationCall.pathParameter(name: String) = parameters.getAll(name)?.joinToString("/") ?: error("Missing path parameter '$name'")

inline fun <reified T : Enum<T>> ApplicationCall.enumParameter(name: String, defaultValue: T? = null): T =
    parameters[name]?.let { value -> enumValues<T>().find { it.name.lowercase() == value.lowercase() } } ?: defaultValue ?: error("Missing or invalid enum parameter '$name'")

inline fun <reified T : Enum<T>> ApplicationCall.enumParameterOrNull(name: String): T? =
    parameters[name]?.let { value -> enumValues<T>().find { it.name.lowercase() == value.lowercase() } }