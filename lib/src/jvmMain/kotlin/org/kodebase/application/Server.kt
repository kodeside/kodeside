package org.kodebase.application

import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*

fun server(port: Int = 8070, configure: ApplicationEngine.Configuration.() -> Unit = {}, module: Application.() -> Unit) {
    embeddedServer(Netty, port = port, configure = configure) {
        module(this)
    }.start(wait = true)
}