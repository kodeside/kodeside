package org.kodebase.application.security

import kotlinx.serialization.Serializable

@Serializable
data class NamedIdentity(val name: String) : PrincipalIdentity