package org.kodebase.application.security

import org.kodebase.security.Identity
import io.ktor.server.auth.*

interface PrincipalIdentity : Principal, Identity