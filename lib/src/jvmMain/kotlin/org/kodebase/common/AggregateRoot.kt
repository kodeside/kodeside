package org.kodebase.common

import org.kodebase.common.model.Entity
import org.kodebase.messaging.event.DomainEvent
import org.kodebase.messaging.event.EventSource

/**
 * Aggregate root represents aggregation of associated domain objects.
 * Aggregate root
 * - has an identity - unique id
 * - provide public api to make changes in the contained domain objects
 * - is always valid and consistent
 * - collects domain events
 */
abstract class AggregateRoot : Entity, EventSource {

    private val mutableEvents: MutableList<DomainEvent> = mutableListOf()

    final override val events: List<DomainEvent> get() = mutableEvents.toList()

    protected fun addEvent(event: DomainEvent) = mutableEvents.add(event)

    override fun clearEvents() = mutableEvents.clear()
}