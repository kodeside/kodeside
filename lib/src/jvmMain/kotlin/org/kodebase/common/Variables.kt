package org.kodebase.common

import org.kodebase.config.Config
import java.io.File

actual val config = Config().apply {
    load("/config.properties")
    load(File("config.properties"))
    load(File("config.local.properties"))
    load(System.getProperties().mapNotNull {
        val name = it.key as String
        if (name.startsWith("config."))
            name.substring(7) to it.value as String
        else
            null
    }.toMap())
}