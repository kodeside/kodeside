package org.kodebase.common.cli

import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.PrintStream

fun textConsole(
    input: BufferedReader = BufferedReader(InputStreamReader(System.`in`)),
    output: PrintStream = System.out,
    beforeInput: () -> Unit = { output.print("> ") },
    afterInput: (String) -> Unit
) {
    while (true) {
        beforeInput()
        val text = input.readLine()!!.trim()
        if (text == "exit")
            break
        afterInput(text)
    }
}
