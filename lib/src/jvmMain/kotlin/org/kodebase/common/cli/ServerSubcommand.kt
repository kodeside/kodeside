package org.kodebase.common.cli

import io.ktor.server.application.*
import io.ktor.server.engine.*
import kotlinx.cli.ArgType
import kotlinx.cli.ExperimentalCli
import kotlinx.cli.Subcommand
import kotlinx.cli.default
import org.kodebase.application.server

@OptIn(ExperimentalCli::class)
open class ServerSubcommand(
    name: String = "server",
    description: String = "Run application server",
    port: Int = 8070,
    private val configure: ApplicationEngine.Configuration.() -> Unit = {},
    private val module: Application.() -> Unit
) : Subcommand(name, description) {

    private val port by option(ArgType.Int, "port", "p", "Server port").default(port)
    override fun execute() = server(port, configure, module)
}