package org.kodebase.common.job

import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.kodein.di.DI
import org.kodein.di.bindSet
import org.kodein.di.bindSingleton
import org.kodein.di.instance
import org.kodebase.application.stringParameter
import org.kodebase.common.repository.MemoryJobLogRepository
import org.kodebase.common.repository.MongoJobLogRepository
import org.kodebase.common.service.JobService
import org.kodebase.di.*

fun DI.Builder.jobBindings(jobLoggingType: JobLoggingType = JobLoggingType.Memory) {
    bindSet<Job>()
    bindSingleton {
        when (jobLoggingType) {
            JobLoggingType.Memory -> MemoryJobLogRepository()
            JobLoggingType.Mongo -> MongoJobLogRepository(instance())
            else -> error("Job logging type $jobLoggingType not supported")
        }
    }
    bindSingleton { new(::JobService) }
}

fun Route.jobRouting() {
    route("/jobs") {
        val jobService by di.instance<JobService>()
        get("/{name}") {
            val name = call.stringParameter("name")
            jobService.run(name)?.let {
                call.respond(it)
            }
        }
        get("/all") {
            jobService.runAll().let {
                call.respond(it)
            }
        }
    }
}