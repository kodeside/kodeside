package org.kodebase.common.mongo

import org.bson.conversions.Bson

class DocumentNotFound(message: String) :
    Exception(message) {
    constructor(
        collection: String,
        filter: Bson,
    ) : this("Document not found [collection: `$collection`, filter: $filter]")

    constructor(
        collection: String,
        id: Any,
    ) : this("Document not found  [collection `$collection`, id: $id")
}
