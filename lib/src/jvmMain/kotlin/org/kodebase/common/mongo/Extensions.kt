package org.kodebase.common.mongo

import com.mongodb.client.MongoCollection
import org.bson.conversions.Bson
import org.litote.kmongo.and
import org.litote.kmongo.findOne
import org.litote.kmongo.findOneById

fun <T> MongoCollection<T>.exists(vararg filters: Bson?): Boolean = countDocuments(and(*filters)) > 0L

fun <T> MongoCollection<T>.getOneById(id: Any): T =
    findOneById(id) ?: throw DocumentNotFound(this.namespace.fullName, id)

fun <T> MongoCollection<T>.getOne(vararg filters: Bson?): T {
    val filter = and(*filters)
    return findOne(filter) ?: throw DocumentNotFound(this.namespace.fullName, filter)
}
