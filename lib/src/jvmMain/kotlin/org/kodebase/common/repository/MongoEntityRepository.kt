package org.kodebase.common.repository

import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoDatabase
import org.bson.conversions.Bson
import org.kodebase.common.model.Entity
import org.kodebase.common.mongo.exists
import org.kodebase.common.mongo.getOneById
import org.kodebase.common.type.ID
import org.litote.kmongo.*
import org.litote.kmongo.util.KMongoUtil
import kotlin.reflect.KClass

open class MongoEntityRepository<E : Entity>(
    protected val database: MongoDatabase,
    protected val model: KClass<E>
) : EntityRepository<E, Bson> {

    protected open val collection: MongoCollection<E> by lazy {
        database.getCollection(collectionName(model), model.java)
    }

    protected fun collectionName(clazz: KClass<*>) = KMongoUtil.defaultCollectionName(clazz)

    override fun get(id: ID): E = collection.getOneById(id)

    override fun exists(id: ID): Boolean = collection.exists(Entity::_id eq id)

    override fun find(id: ID): E? = collection.findOneById(id)

    override fun find(filter: Bson) = collection.find(filter).toList()

    override fun find(ids: List<ID>) = collection.find(Entity::_id `in` ids).toList()

    override fun save(entity: E) =
        collection.updateOneById(entity._id, entity, upsert()).run {
            modifiedCount > 0 || upsertedId != null
        }

    override fun delete(id: ID) =
        collection.deleteOneById(id).deletedCount > 0

    override val count get() = collection.countDocuments().toInt()

    override fun count(filter: Bson) = collection.countDocuments(filter).toInt()
}