package org.kodebase.common.repository

import com.mongodb.client.MongoDatabase
import org.bson.conversions.Bson
import org.kodebase.common.model.JobLog
import org.litote.kmongo.and
import org.litote.kmongo.eq

class MongoJobLogRepository(database: MongoDatabase) : JobLogRepository<Bson>, MongoEntityRepository<JobLog>(database, JobLog::class) {

    override fun findBy(name: String, status: JobLog.Status) =
        collection.find(and(JobLog::name eq name, JobLog::status eq status)).lastOrNull()
}