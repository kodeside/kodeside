package org.kodebase.common.storage

import io.ktor.server.plugins.*
import org.kodebase.util.LoggerDelegate
import software.amazon.awssdk.auth.credentials.AwsCredentials
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.core.sync.ResponseTransformer
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.*
import java.io.InputStream
import java.io.OutputStream
import java.net.URI

class AmazonS3Storage(
    endpoint: URI?,
    accessKey: String,
    secretKey: String,
    region: String,
    private val bucket: String
): FileStorage {

    private val s3 = with(S3Client.builder()) {
        if (accessKey != "none" && secretKey != "none")
            credentialsProvider {
                object : AwsCredentials {
                    override fun accessKeyId() = accessKey
                    override fun secretAccessKey() = secretKey
                }
            }
        endpoint?.let {
            endpointOverride(it)
        }
        region(Region.of(region))
        build()
    }
    private val logger by LoggerDelegate()

    init {
        logger.info("Created with bucket $bucket")
    }

    override fun read(path: String, output: OutputStream) {
        s3.getObject(
            GetObjectRequest.builder()
                .bucket(bucket)
                .key(path)
                .build(),
            ResponseTransformer.toOutputStream(output)
        )
    }

    override fun get(path: String): StorageFile {
        val objectRequest = GetObjectRequest.builder()
            .bucket(bucket)
            .key(path)
            .build()
        try {
            s3.getObject(objectRequest).use {
                val metadata = it.response().metadata().toSortedMap(java.lang.String.CASE_INSENSITIVE_ORDER)
                val timeCreated = metadata.remove("time_created")?.toLong() ?: 0L
                return StorageFile(
                    path, it.response().contentLength(), it.response().contentType(),
                    timeCreated, it.response().lastModified().toEpochMilli(), metadata
                )
            }
        } catch (e: NoSuchKeyException) {
            throw NotFoundException("File $path not found in bucket $bucket")
        }
    }

    override fun write(path: String, contentType: String, input: InputStream) {
        val putObjectRequest = PutObjectRequest.builder()
            .bucket(bucket)
            .key(path)
            .build()
        s3.putObject(putObjectRequest, RequestBody.fromContentProvider({ input }, contentType))
    }

    override fun delete(path: String): Boolean {
        val deleteObjectRequest = DeleteObjectRequest.builder()
            .bucket(bucket)
            .key(path)
            .build()

        s3.deleteObject(deleteObjectRequest)
        return true
    }

    override fun toString() = "${this::class.simpleName}(bucket=$bucket)"
}