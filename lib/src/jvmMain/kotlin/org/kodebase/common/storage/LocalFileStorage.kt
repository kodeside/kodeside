package org.kodebase.common.storage

import io.ktor.server.plugins.*
import org.kodebase.util.LoggerDelegate
import java.io.*
import java.nio.file.Files
import java.nio.file.attribute.BasicFileAttributes
import java.util.*

class LocalFileStorage(private val base: File) : FileStorage {

    private val logger by LoggerDelegate()

    init {
        logger.info("Created with base $base")
    }

    override fun read(path: String, output: OutputStream) {
        val file = File(base, path)
        if (!file.exists())
            throw NotFoundException("File $path not found")
        FileInputStream(file).copyTo(output)
    }

    override fun get(path: String): StorageFile {
        val file = File(base, path)
        if (file.exists()) {
            val attr = Files.readAttributes(file.toPath(),  BasicFileAttributes::class.java)
            val properties = Properties()
            val propertiesFile = File(base, "$path.properties")
            val contentType = (if (propertiesFile.exists()) {
                properties.load(FileInputStream(propertiesFile))
                properties.remove("contentType") as String
            } else null) ?: FileStorage.defaultContentType
            @Suppress("UNCHECKED_CAST")
            return StorageFile(file.name, file.length(), contentType,
                attr.creationTime().toMillis(), attr.lastModifiedTime().toMillis(), properties as Map<String, String>)
        } else {
            throw NotFoundException("File $path not found")
        }
    }

    override fun write(path: String, contentType: String, input: InputStream) {
        val file = File(base, path)
        val propertiesFile = File(base, "$path.properties")
        val properties = Properties()
        properties.setProperty("contentType", contentType)
        file.parentFile.mkdirs()
        FileOutputStream(propertiesFile).use { properties.store(it, null) }
        val output = FileOutputStream(file)
        input.copyTo(output)
    }

    override fun delete(path: String) = File(base, path).delete()

    override fun toString(): String = "${this::class.simpleName}(base=$base)"

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val storage = LocalFileStorage(File("/Users/tomas.zajicek/Downloads/storage"))
            storage.write("test1", "text/plain", "Test...".toByteArray())
            println(storage.get("test1"))
            storage.read("test1", System.out)
        }
    }
}