package org.kodebase.common.type

inline fun List<LocalizedText>.localize(language: String) = firstOrNull { it.language == language }?.text ?: firstOrNull { it.language == "en" }?.text ?: first().text
