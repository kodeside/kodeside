package org.kodebase.common.type

import kotlinx.serialization.Serializable
import org.bson.types.ObjectId

@Serializable(with = IdSerializer::class)
actual interface ID

actual fun newId(): ID = WrappedObjectId(ObjectId())

actual fun String.toID(): ID = WrappedObjectId(this)
