package org.kodebase.common.type

import com.github.jershell.kbson.FlexibleDecoder
import com.github.jershell.kbson.ObjectIdSerializer
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.JsonEncoder
import org.bson.AbstractBsonReader
import org.bson.BsonType
import org.bson.types.ObjectId
import org.litote.kmongo.id.IdGenerator
import org.litote.kmongo.id.ObjectIdGenerator

class IdSerializer<T : ID> : KSerializer<T> {

    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("IdSerializer", PrimitiveKind.STRING)

    @Suppress("UNCHECKED_CAST")
    override fun deserialize(decoder: Decoder): T =
        (if (decoder is JsonDecoder)
            WrappedObjectId(decoder.decodeString())
        else
            WrappedObjectId(deserializeObjectId(decoder as FlexibleDecoder) as ObjectId)) as T

    private fun deserializeObjectId(decoder: FlexibleDecoder): Any {
        val alreadyRead = decoder.alreadyReadId
        return if (alreadyRead != null) {
            decoder.alreadyReadId = null
            alreadyRead
        } else if (decoder.reader.state == AbstractBsonReader.State.NAME) {
            val keyId = decoder.reader.readName()
            if (IdGenerator.defaultGenerator != ObjectIdGenerator) keyId else ObjectId(keyId)
        } else {
            when (decoder.reader.currentBsonType) {
                BsonType.STRING -> decoder.decodeString()
                BsonType.OBJECT_ID -> decoder.reader.readObjectId()
                else -> throw SerializationException("Unsupported ${decoder.reader.currentBsonType} when reading _id")
            }
        }
    }

    override fun serialize(encoder: Encoder, value: T) {
        when (value) {
//                is String -> encoder.encodeString(it)
            is WrappedObjectId ->
                if (encoder is JsonEncoder)
                    encoder.encodeString(value.id.toString())
                else
                    ObjectIdSerializer.serialize(encoder, value.id)
            else -> error("unsupported id type $value")
        }
    }
}
