package org.kodebase.common.type

import kotlinx.serialization.Serializable

@Serializable
data class LocalizedText(val language: String, val text: String) {

    override fun toString() = "$language:$text"
}