package org.kodebase.common.type

enum class Sex { None, Male, Female }