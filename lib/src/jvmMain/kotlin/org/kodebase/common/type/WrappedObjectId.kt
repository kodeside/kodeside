package org.kodebase.common.type

import kotlinx.serialization.Serializable
import org.bson.types.ObjectId

@Serializable(with = IdSerializer::class)
data class WrappedObjectId(val id: ObjectId) : ID {
    constructor(id: String) : this(ObjectId(id))

    override fun toString(): String = id.toString()
}
