package org.kodebase.config

import org.kodebase.runtime.getenv
import org.kodebase.util.LoggerDelegate
import java.io.File
import java.io.FileReader
import java.util.*

actual class Config {

    private val properties = Properties()
    private val logger by LoggerDelegate()

    fun load(file: File) {
        if (file.exists()) {
            logger.info("Loading config file ${file.absolutePath}")
            properties.load(FileReader(file))
        }
    }

    fun load(path: String) = javaClass.getResourceAsStream(path)?.let {
        logger.info("Loading config resource $path")
        properties.load(it)
    }

    fun load(map: Map<String, String>) = map.forEach {
        properties.setProperty(it.key, it.value)
    }

    actual fun getOrNull(name: String): String? = properties[name] as? String ?: getenv(name)

    actual operator fun get(name: String) = getOrNull(name) ?: error("Config property $name not found")

    actual fun getOrDefault(name: String, default: String) = getOrNull(name) ?: default
}