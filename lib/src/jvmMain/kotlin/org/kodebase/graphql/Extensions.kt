package org.kodebase.graphql

import com.apurebase.kgraphql.Context
import com.apurebase.kgraphql.GraphQLLazy
import com.apurebase.kgraphql.schema.dsl.SchemaBuilder
import kotlinx.datetime.*
import org.bson.types.ObjectId
import org.kodebase.application.security.PrincipalIdentity
import org.kodebase.common.type.ID
import org.kodebase.common.type.WrappedObjectId

fun schema(block: SchemaBuilder.() -> Unit) = GraphQLLazy.addBuilder(block)

fun SchemaBuilder.scalarInstant() = stringScalar<Instant> {
    description = "The Instant scalar type represents a date/time value in ISO-6801 format in UTC "
    deserialize = { Instant.parse(it) }
    serialize = { it.toString() }
}

fun SchemaBuilder.scalarLocalDate() = stringScalar<LocalDate> {
    description = "The LocalDate scalar type represents a date value in ISO-6801 format at the start of the day(0h 0m 0s) in UTC"
//  coercion = LocalDateCoercion() //some bug in KGraphlQL
    deserialize = { Instant.parse(it).toLocalDateTime(TimeZone.UTC).date }
    serialize = { it.atStartOfDayIn(TimeZone.UTC).toString() }
}

fun SchemaBuilder.scalarID() = stringScalar<ID> {
    deserialize = { WrappedObjectId(ObjectId(it)) }
    serialize = { it.toString() }
}

inline val Context.identity get() = identityOrNull ?: error("Missing identity in GraphQL context")
inline val Context.identityOrNull get() = get<PrincipalIdentity>()