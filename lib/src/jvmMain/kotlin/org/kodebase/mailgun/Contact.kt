package org.kodebase.mailgun

@Suppress("MemberVisibilityCanBePrivate")
class Contact(val email: String, val name: String?) {

    override fun toString() = name?.let { "$name <$email>" } ?: email
}
