package org.kodebase.mapper

import org.kodebase.mapper.datetime.KotlinXDateTimeModule
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.util.StdDateFormat
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.litote.kmongo.id.jackson.IdJacksonModule
import java.util.*

object ObjectUtil {

    @JvmStatic
    val defaultMapper: ObjectMapper = configureMapper(ObjectMapper())
    @JvmStatic
    val xmlMapper: XmlMapper = configureMapper(XmlMapper()) {
        setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
    }
    @JvmStatic
    val yamlMapper: YAMLMapper = configureMapper(YAMLMapper())

    fun createMapper() = configureMapper(ObjectMapper()) // Backward compatibility

    private fun <T : ObjectMapper> configureMapper(mapper: T, block: (T.() -> Unit)? = null) = mapper
        .registerModule(KotlinModule())
        .registerModule(IdJacksonModule())
        .registerModule(JavaTimeModule())
        .registerModule(KotlinXDateTimeModule())
        .setDateFormat(StdDateFormat().withTimeZone(TimeZone.getTimeZone("UTC")))
        .configure(SerializationFeature.INDENT_OUTPUT, true)
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .apply {
                setConfig(serializationConfig.withView(Any::class.java))
                block?.invoke(mapper)
            } as T
}
