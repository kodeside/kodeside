package org.kodebase.mapper.datetime

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.core.util.JsonParserSequence
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import kotlinx.datetime.Instant

internal class InstantDeserializer : JsonDeserializer<Instant?>() {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): Instant? {

        val parser = if (p is JsonParserSequence) p.delegate() else p

        return when (parser.currentToken) {
            JsonToken.VALUE_NULL -> null
            JsonToken.VALUE_STRING -> {
                val time = parser.readValueAs(String::class.java)
                Instant.parse(time)
            }
            else -> error("Can not deserialize to kotlinx.datetime.Instant")
        }
    }
}
