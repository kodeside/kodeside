package org.kodebase.mapper.datetime

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import kotlinx.datetime.Instant
import kotlinx.serialization.json.Json

internal class InstantSerializer : JsonSerializer<Instant?>() {
    override fun serialize(value: Instant?, gen: JsonGenerator, serializers: SerializerProvider) {
        if (value == null) {
            gen.writeNull()
        } else {
            gen.writeString(Json.encodeToString(Instant.serializer(), value).removeSurrounding("\""))
        }
    }
}
