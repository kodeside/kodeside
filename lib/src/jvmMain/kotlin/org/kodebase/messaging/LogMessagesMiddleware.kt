package org.kodebase.messaging

import org.kodebase.util.LoggerDelegate

class LogMessagesMiddleware : MessageBusMiddleware {

    private val logger by LoggerDelegate()

    override fun process(envelope: Envelope<Message<*>, *>): Envelope<*, *> = envelope.apply {
        val message = envelope.message
        logger.info("[${message.typeName}] $message")
    }
}