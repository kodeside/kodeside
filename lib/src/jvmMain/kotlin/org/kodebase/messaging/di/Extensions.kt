package org.kodebase.messaging.di

import kotlinx.serialization.StringFormat
import org.kodebase.di.new
import org.kodebase.messaging.*
import org.kodebase.messaging.command.*
import org.kodebase.messaging.event.*
import org.kodebase.messaging.provider.*
import org.kodebase.messaging.query.*
import org.kodebase.messaging.result
import org.kodein.di.*
import org.kodein.di.bindings.DIBinding
import org.kodebase.di.di as dic
import javax.jms.ConnectionFactory
import javax.jms.QueueConnectionFactory
import javax.jms.TopicConnectionFactory

inline fun DI.Builder.messagingBindings() {
    bindSet<MessageBusMiddleware>()
    bindSet<CommandHandler<*, *>>()
    bindSet<QueryHandler<*, *>>()
    bindSet<EventHandler<*>>()
}

inline fun DI.Builder.messageBusLogging() {
    busMiddleware { singleton { new(::LogMessagesMiddleware) } }
}

inline fun DI.Builder.localMessaging() {
    messagingBindings()
    bindSingleton<CommandBus> { LocalCommandBus(dic, instance(), instance()) }
    bindSingleton<QueryBus> { LocalQueryBus(dic, instance(), instance()) }
    bindSingleton<EventBus> { LocalEventBus(dic, instance(), instance()) }
}

inline fun DI.Builder.jmsMessaging(
    queueConnectionFactory: QueueConnectionFactory,
    topicConnectionFactory: TopicConnectionFactory,
    stringFormat: StringFormat
) {
    messagingBindings()
    bindSingleton<CommandBus> { JMSCommandBus(queueConnectionFactory, instance(), provider(), stringFormat, instance()) }
    bindSingleton<QueryBus> { JMSQueryBus(queueConnectionFactory, instance(), provider(), stringFormat, instance()) }
    bindSingleton<EventBus> { JMSEventBus(topicConnectionFactory, instance(), provider(), stringFormat, instance()) }
}

inline fun DI.Builder.jmsMessaging(connectionFactory: ConnectionFactory, stringFormat: StringFormat) =
    jmsMessaging(
        connectionFactory as QueueConnectionFactory,
        connectionFactory as TopicConnectionFactory,
        stringFormat
    )

inline fun DI.Builder.eventHandler(
    tag: Any? = null,
    overrides: Boolean? = null,
    creator: () -> DIBinding<*, *, EventHandler<*>>,
) = inSet(tag, overrides, creator)

inline fun DI.Builder.queryHandler(
    tag: Any? = null,
    overrides: Boolean? = null,
    creator: () -> DIBinding<*, *, QueryHandler<*, *>>,
) = inSet(tag, overrides, creator)

inline fun DI.Builder.commandHandler(
    tag: Any? = null,
    overrides: Boolean? = null,
    creator: () -> DIBinding<*, *, CommandHandler<*, *>>,
) = inSet(tag, overrides, creator)
inline fun DI.Builder.busMiddleware(
    tag: Any? = null,
    overrides: Boolean? = null,
    creator: () -> DIBinding<*, *, MessageBusMiddleware>,
) = inSet(tag, overrides, creator)


inline fun <reified I, reified C : Command<R>, reified R : Any> DI.Builder.commandHandler(crossinline block: I.(Envelope<C, R>) -> R) {
    commandHandler {
        singleton {
            object : CommandHandler<C, R> {
                override val canHandle = C::class
                override fun handle(envelope: Envelope<C, R>) = result {
                    block(instance(), envelope)
                }
            }
        }
    }
}

inline fun <reified I, reified Q : Query<R>, reified R> DI.Builder.queryHandler(crossinline block: I.(Envelope<Q, R>) -> R) {
    queryHandler {
        singleton {
            object : QueryHandler<Q, R> {
                override val canHandle = Q::class
                override fun handle(envelope: Envelope<Q, R>) = result {
                    block(instance(), envelope)
                }
            }
        }
    }
}

inline fun <reified I, reified E : Event> DI.Builder.eventHandler(crossinline block: I.(Envelope<E, Unit>) -> Unit) {
    eventHandler {
        singleton {
            object : EventHandler<E> {
                override val canHandle = E::class
                override fun handle(envelope: Envelope<E, Unit>) = result {
                    block(instance(), envelope)
                }
            }
        }
    }
}
