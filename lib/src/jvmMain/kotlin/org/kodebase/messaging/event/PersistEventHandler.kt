package org.kodebase.messaging.event

import org.kodebase.messaging.Envelope
import org.kodebase.messaging.TimeStamp
import org.kodebase.messaging.event
import org.kodebase.messaging.getStamp
import org.kodebase.messaging.model.EventLog
import org.kodebase.messaging.repository.EventLogRepository
import org.kodebase.messaging.result

class PersistEventHandler(
    private val eventLogRepository: EventLogRepository<*>
): EventHandler<Event> {
    override val canHandle = Event::class

    override fun handle(envelope: Envelope<Event, Unit>) = result {
        val eventLog = EventLog.create(envelope.event, envelope.getStamp<TimeStamp>().time, null)
        eventLogRepository.save(eventLog)
        Unit
    }
}