package org.kodebase.messaging.model

import org.kodebase.common.AggregateRoot
import org.kodebase.common.type.newId
import org.kodebase.messaging.event.Event
import org.kodebase.security.Identity
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
class EventLog private constructor(
    val event: Event,
    val eventTime: Instant,
    val identity: Identity? = null,
) : AggregateRoot() {

    override val _id = newId()
    companion object {
        fun create(event: Event, time: Instant, identity: Identity?) = EventLog(event, time, identity)
    }
}