package org.kodebase.messaging.provider

import kotlinx.serialization.*
import org.kodebase.common.exception.ApplicationException
import org.kodebase.messaging.Envelope
import org.kodebase.messaging.Message
import org.kodebase.messaging.MessageHandler
import org.kodebase.messaging.event.EventHandler
import org.kodebase.monitoring.Monitoring
import org.kodebase.util.LoggerDelegate
import javax.jms.DeliveryMode
import javax.jms.Session
import javax.jms.TextMessage
import kotlin.reflect.KClass
import kotlin.reflect.full.createType

abstract class AbstractJMSBus<M : Message<R>, R, MH : MessageHandler<M, R>>(
    open val monitoring: Monitoring,
    open val stringFormat: StringFormat,
    open val deliveryMode: Int = DeliveryMode.NON_PERSISTENT
) : AbstractMessageBus<M, R, ApplicationException>(), AutoCloseable
{
    val prefix = "kodebase"

    protected val logger by LoggerDelegate()

    fun Session.handle(handler: MH, name: String, text: String) {
        val messageType = handler.canHandle.createType()
        val serializer = stringFormat.serializersModule.serializer(messageType)
        //val serializer = handler.canHandle.serializer()
        val message = stringFormat.decodeFromString(serializer, text) as M
        logger.info("Handling $message")
        val envelope = fold(message, emptyArray()) //TODO create stamps from JMS message properties
        try {
            val result = handler.handle(envelope)
            if (handler !is EventHandler<*>)
                send(name, result.encoder.encodeToString(stringFormat), result.isFailure)
        } catch (exception: Throwable) {
            logger.error("Message handling failed", exception)
        }
    }

//    private fun <T : Any> KClass<T>.serializer(): KSerializer<T> {
//        val javaClass = this::class.java.classLoader.loadClass("$qualifiedName\$\$serializer")
//        val kotlinClass = Reflection.createKotlinClass(javaClass)
//        return kotlinClass.objectInstance as KSerializer<T>
//    }

    protected fun <MM : Message<RR>, RR : Any> Session.createMessage(envelope: Envelope<*, *>, messageClass: KClass<MM>): TextMessage {
        //val serializer = messageClass.serializer()
        val serializer = stringFormat.serializersModule.serializer(messageClass.createType())
        val text = stringFormat.encodeToString(serializer, envelope.message as MM)
        //TODO set stamp values to JMS message properties
        return createTextMessage(text)
    }

    private fun Session.send(name: String, text: String, isFailure: Boolean) {
        val queueName = "$prefix:$name/result"
        val queue = createQueue(queueName)
        val message = createTextMessage(text)
        message.setBooleanProperty("isFailure", isFailure)
        val producer = createProducer(queue)
        producer.deliveryMode = deliveryMode
        logger.info("Sending $name ${if (isFailure) "failure" else "success"} result = $text")
        producer.send(message)
    }
}