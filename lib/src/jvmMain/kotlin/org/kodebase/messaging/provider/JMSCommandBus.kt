package org.kodebase.messaging.provider

import kotlinx.serialization.StringFormat
import org.kodebase.common.exception.ApplicationException
import org.kodebase.messaging.*
import org.kodebase.messaging.command.Command
import org.kodebase.messaging.command.CommandBus
import org.kodebase.messaging.command.CommandHandler
import org.kodebase.monitoring.Monitoring
import javax.jms.*

class JMSCommandBus(
    queueConnectionFactory: QueueConnectionFactory,
    monitoring: Monitoring,
    handlerProvider: () -> Set<CommandHandler<Command<Any>, Any>>,
    stringFormat: StringFormat,
    override val middlewares: Set<MessageBusMiddleware>,
    override val failureHandler: FailureHandler<Command<Any>, Any, ApplicationException> = ApplicationFailureHandler(),
    resultTimeout: Long = 5000,
    transacted: Boolean = false,
    acknowledgeMode: Int = Session.AUTO_ACKNOWLEDGE,
    deliveryMode: Int = DeliveryMode.NON_PERSISTENT
) : CommandBus, JMSQueueBus<Command<Any>, Any, CommandHandler<Command<Any>, Any>>(
    queueConnectionFactory, monitoring, handlerProvider, stringFormat, resultTimeout, transacted, acknowledgeMode, deliveryMode
)