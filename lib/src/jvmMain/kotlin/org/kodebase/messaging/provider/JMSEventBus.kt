package org.kodebase.messaging.provider

import kotlinx.serialization.StringFormat
import org.kodebase.common.exception.ApplicationException
import org.kodebase.messaging.*
import org.kodebase.messaging.event.Event
import org.kodebase.messaging.event.EventBus
import org.kodebase.messaging.event.EventHandler
import org.kodebase.monitoring.Monitoring
import javax.jms.DeliveryMode
import javax.jms.Session
import javax.jms.TopicConnectionFactory

class JMSEventBus(
    topicConnectionFactory: TopicConnectionFactory,
    monitoring: Monitoring,
    handlerProvider: () -> Set<EventHandler<Event>>,
    stringFormat: StringFormat,
    override val middlewares: Set<MessageBusMiddleware>,
    override val failureHandler: FailureHandler<Event, Unit, ApplicationException> = ApplicationFailureHandler(),
    transacted: Boolean = false,
    acknowledgeMode: Int = Session.AUTO_ACKNOWLEDGE,
    deliveryMode: Int = DeliveryMode.NON_PERSISTENT
) : EventBus, JMSTopicBus<Event, Unit, EventHandler<Event>>(
    topicConnectionFactory, monitoring, handlerProvider, stringFormat, transacted, acknowledgeMode, deliveryMode
)