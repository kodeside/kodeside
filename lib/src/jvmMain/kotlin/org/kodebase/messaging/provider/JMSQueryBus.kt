package org.kodebase.messaging.provider

import kotlinx.serialization.StringFormat
import org.kodebase.common.exception.ApplicationException
import org.kodebase.messaging.*
import org.kodebase.messaging.query.Query
import org.kodebase.messaging.query.QueryBus
import org.kodebase.messaging.query.QueryHandler
import org.kodebase.monitoring.Monitoring
import javax.jms.DeliveryMode
import javax.jms.QueueConnectionFactory
import javax.jms.Session

class JMSQueryBus(
    queueConnectionFactory: QueueConnectionFactory,
    monitoring: Monitoring,
    handlerProvider: () -> Set<QueryHandler<Query<Any>, Any>>,
    stringFormat: StringFormat,
    override val middlewares: Set<MessageBusMiddleware>,
    override val failureHandler: FailureHandler<Query<Any>, Any, ApplicationException> = ApplicationFailureHandler(),
    resultTimeout: Long = 5000,
    transacted: Boolean = false,
    acknowledgeMode: Int = Session.AUTO_ACKNOWLEDGE,
    deliveryMode: Int = DeliveryMode.NON_PERSISTENT
) : QueryBus, JMSQueueBus<Query<Any>, Any, QueryHandler<Query<Any>, Any>>(
    queueConnectionFactory, monitoring, handlerProvider, stringFormat, resultTimeout, transacted, acknowledgeMode, deliveryMode
)