package org.kodebase.messaging.provider

import kotlinx.serialization.*
import org.kodebase.common.exception.ApplicationException
import org.kodebase.messaging.*
import org.kodebase.messaging.Message
import org.kodebase.monitoring.Monitoring
import javax.jms.*
import kotlin.reflect.KType
import kotlin.reflect.full.hasAnnotation

abstract class JMSQueueBus<M : Message<R>, R, MH : MessageHandler<M, R>>(
    queueConnectionFactory: QueueConnectionFactory,
    monitoring: Monitoring,
    handlerProvider: () -> Set<MH>,
    override val stringFormat: StringFormat,
    private val resultTimeout: Long = 5000,
    private val transacted: Boolean = false,
    private val acknowledgeMode: Int = Session.AUTO_ACKNOWLEDGE,
    override val deliveryMode: Int = DeliveryMode.NON_PERSISTENT
) : AbstractJMSBus<M, R, MH>(monitoring, stringFormat, deliveryMode), AutoCloseable {

    private val dispatchConnection: QueueConnection = queueConnectionFactory.createQueueConnection()
    private val handlerConnection: QueueConnection = queueConnectionFactory.createQueueConnection()

    init {
        handlerProvider().forEach { handler ->
            val name = handler.canHandle.qualifiedName!!
            val queueName = "$prefix:$name"
            with(handlerConnection.createQueueSession(transacted, acknowledgeMode)) {
                val queue = createQueue(queueName)
                val consumer = createConsumer(queue)
                consumer.messageListener = MessageListener {
                    monitoring.performanceTransaction(this::class.qualifiedName!!, handler::class.qualifiedName!!, null/*userPrincipalOrNull*/) {
                        handle(handler, name, (it as TextMessage).text)
                    }
                }
            }
            logger.info("${handler::class.qualifiedName} is listening on $name")

        }
        dispatchConnection.start()
        handlerConnection.start()
    }

    override fun close() {
        dispatchConnection.close()
        handlerConnection.close()
    }

    override fun <MM : Message<RR>, RR> dispatch(message: Message<RR>, resultType: KType, vararg stamp: Stamp): RR {
        val name = message::class.qualifiedName!!
        val envelope = fold(message as M, stamp)
        return monitoring.performanceTransaction(this::class.qualifiedName!!, name, envelope.userPrincipalOrNull) {
            try {
                dispatchConnection.createQueueSession(transacted, acknowledgeMode).use { session ->
                    val queueName = "$prefix:$name"
                    val queue = session.createQueue(queueName)
                    val producer = session.createProducer(queue)
                    producer.deliveryMode = deliveryMode
                    session.createMessage(envelope, (message as? Message<Any>)!!::class).let {
                        logger.info("Sending $name ${message.typeName.lowercase()} = ${it.text}")
                        producer.send(it)
                        if (message::class.hasAnnotation<Async>())
                            Unit as RR
                        else
                            session.receive<RR>(name, resultType).also { result ->
                                logger.info("Received $name ${message.typeName.lowercase()} result = $result")
                            }
                    }
                }
            } catch (exception: Throwable) {
                exception.printStackTrace()
                throw failureHandler.handle(envelope, exception)
            }
        }
    }

    private fun <RR> QueueSession.receive(name: String, resultType: KType): RR {
        val queueName = "$prefix:$name/result"
        val queue = createQueue(queueName)
        val consumer = createConsumer(queue)
        val message = consumer.receive(resultTimeout)
        return (message as? TextMessage)?.run {
            if (message.getBooleanProperty("isFailure")) {
                val failure = stringFormat.decodeFromString<MessageHandlerFailure>(text)
                throw ApplicationException(failure.message ?: "Unknown", failure.language, null, *failure.arguments.map { it.value }.toTypedArray())
            } else {
                val serializer = stringFormat.serializersModule.serializer(resultType)
                stringFormat.decodeFromString(serializer, text) as RR
            }
        } ?: error("No $name result received in $resultTimeout ms")
    }
}