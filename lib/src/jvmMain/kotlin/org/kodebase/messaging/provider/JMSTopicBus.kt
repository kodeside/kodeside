package org.kodebase.messaging.provider

import kotlinx.serialization.StringFormat
import kotlinx.serialization.json.Json
import org.kodebase.messaging.*
import org.kodebase.messaging.Message
import org.kodebase.monitoring.Monitoring
import javax.jms.*
import kotlin.reflect.KType

abstract class JMSTopicBus<M : Message<R>, R, MH : MessageHandler<M, R>>(
    topicConnectionFactory: TopicConnectionFactory,
    monitoring: Monitoring,
    handlerProvider: () -> Set<MH>,
    override val stringFormat: StringFormat = Json { prettyPrint = true },
    private val transacted: Boolean = false,
    private val acknowledgeMode: Int = Session.AUTO_ACKNOWLEDGE,
    override val deliveryMode: Int = DeliveryMode.NON_PERSISTENT
) : AbstractJMSBus<M, R, MH>(monitoring, stringFormat, deliveryMode), AutoCloseable {

    private val dispatchConnection: TopicConnection = topicConnectionFactory.createTopicConnection()
    private val handlerConnection: TopicConnection = topicConnectionFactory.createTopicConnection()

    init {
        handlerProvider().forEach { handler ->
            val name = handler.canHandle.qualifiedName!!
            val topicName = "$prefix:$name"
            with(handlerConnection.createTopicSession(transacted, acknowledgeMode)) {
                val topic = createTopic(topicName)
                val subscriber = createSubscriber(topic)
                subscriber.messageListener = MessageListener {
                    monitoring.performanceTransaction(this::class.qualifiedName!!, handler::class.qualifiedName!!, null/*userPrincipalOrNull*/) {
                        handle(handler, name, (it as TextMessage).text)
                    }
                }
            }
            logger.info("${handler::class.qualifiedName} is listening on $name")
        }
        dispatchConnection.start()
        handlerConnection.start()
    }

    override fun close() {
        dispatchConnection.close()
        handlerConnection.close()
    }

    override fun <MM : Message<RR>, RR> dispatch(message: Message<RR>, resultType: KType, vararg stamp: Stamp): RR {
        val name = message::class.qualifiedName!!
        val envelope = fold(message as M, stamp)
        return monitoring.performanceTransaction(this::class.qualifiedName!!, name, envelope.userPrincipalOrNull) {
            try {
                dispatchConnection.createTopicSession(transacted, acknowledgeMode).use { session ->
                    val topicName = "$prefix:$name"
                    val topic = session.createTopic(topicName)
                    val publisher = session.createPublisher(topic)
                    publisher.deliveryMode = deliveryMode
                    logger.info("Sending ${message.typeName.lowercase()} ${envelope.message}")
                    session.createMessage(envelope, (message as Message<Any>)!!::class).let {
                        publisher.send(it)
                    }
                }
                Unit as RR
            } catch (exception: Throwable) {
                exception.printStackTrace()
                throw failureHandler.handle(envelope, exception)
            }
        }
    }
}