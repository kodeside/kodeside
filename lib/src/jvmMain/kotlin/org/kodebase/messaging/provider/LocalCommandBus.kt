package org.kodebase.messaging.provider

import org.kodein.di.DI
import org.kodein.di.instance
import org.kodebase.common.exception.ApplicationException
import org.kodebase.messaging.ApplicationFailureHandler
import org.kodebase.messaging.FailureHandler
import org.kodebase.messaging.MessageBusMiddleware
import org.kodebase.messaging.command.Command
import org.kodebase.messaging.command.CommandBus
import org.kodebase.messaging.command.CommandHandler
import org.kodebase.monitoring.Monitoring

class LocalCommandBus(
//    handlerProvider: () -> Set<CommandHandler<Command<Any>, Any>>,
    di: DI,
    override val monitoring: Monitoring,
    override val middlewares: Set<MessageBusMiddleware>,
    override val failureHandler: FailureHandler<Command<Any>, Any, ApplicationException> = ApplicationFailureHandler()
) : LocalMessageBus<Command<Any>, Any>(), CommandBus {
    override val handlers: Set<CommandHandler<Command<Any>, Any>> by di.instance()
}
