package org.kodebase.messaging.provider

import org.kodebase.messaging.*
import org.kodebase.messaging.event.Event
import org.kodebase.messaging.event.EventBus
import org.kodebase.util.LoggerDelegate
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.collect
import org.kodein.di.DI
import org.kodein.di.instance
import org.kodebase.common.exception.ApplicationException
import org.kodebase.monitoring.Monitoring
import kotlin.reflect.KType

class LocalEventBus(
//    handlerProvider: () -> Set<EventHandler<Event>>,
    di:DI,
    override val monitoring: Monitoring,
    override val middlewares: Set<MessageBusMiddleware>,
    override val failureHandler: FailureHandler<Event, Unit, ApplicationException> = ApplicationFailureHandler()
) : LocalMessageBus<Event, Unit>(), EventBus {

    private val logger by LoggerDelegate()

    override val handlers: Set<MessageHandler<Event, Unit>> by di.instance() //lazy(handlerProvider)

    private val scope = CoroutineScope(Dispatchers.Default + SupervisorJob())

    private val _events =
        MutableSharedFlow<Envelope<Event, Unit>>(extraBufferCapacity = 100) // private mutable shared flow
    val events = _events.asSharedFlow() // publicly exposed as read-only shared flow

    override fun <MM : Message<RR>, RR> dispatch(message: Message<RR>, resultType: KType, vararg stamp: Stamp): RR {
        val envelope: Envelope<Event, Unit> = wrap(message as Event, stamp)


        scope.launch {
            _events.emit(envelope) // suspends until event is collected
        }
        return Unit as RR
    }

    init {
        scope.launch(CoroutineName("Event Bus")) {
            _events.collect { event ->
                scope.launch {
                    getHandlers(event.message).forEach {
                        val result = it.handle(event)
                        if (result.isFailure) {
                            logger.error("Event handler failed ${it::class.simpleName}: ${result.exceptionOrNull()?.message}\n ${result.exceptionOrNull()?.stackTraceToString()}")
                        }
                    }
                }
            }
        }
    }
}
