package org.kodebase.messaging.provider

import org.kodebase.common.exception.ApplicationException
import org.kodebase.messaging.*
import org.kodebase.monitoring.Monitoring
import kotlin.reflect.KType
import kotlin.reflect.full.isSuperclassOf

abstract class LocalMessageBus<M : Message<R>, R> : AbstractMessageBus<M, R, ApplicationException>() {

    abstract val monitoring: Monitoring
    abstract val handlers: Set<MessageHandler<M, R>>

    override fun <MM : Message<RR>, RR> dispatch(message: Message<RR>, resultType: KType, vararg stamp: Stamp): RR {
        val envelope = fold(message as M, stamp)
        val handler = getHandlers(envelope.message).singleOrNull() ?: error("Missing handler for ${message::class}")
        return try {
            monitoring.performanceTransaction(this::class.qualifiedName!!, handler.canHandle.qualifiedName!!, envelope.userPrincipalOrNull) {
                handler.handle(envelope).getOrThrow() as RR
            }
        } catch (exception: Throwable) {
            monitoring.capture(this::class.qualifiedName!!, exception)
            throw failureHandler.handle(envelope, exception)
        }
    }

    fun getHandlers(message: Message<R>) = handlers.filter { it.canHandle.isSuperclassOf(message::class) }
}
