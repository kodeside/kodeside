package org.kodebase.messaging.provider

import org.kodein.di.DI
import org.kodein.di.instance
import org.kodebase.common.exception.ApplicationException
import org.kodebase.messaging.ApplicationFailureHandler
import org.kodebase.messaging.FailureHandler
import org.kodebase.messaging.MessageBusMiddleware
import org.kodebase.messaging.query.Query
import org.kodebase.messaging.query.QueryBus
import org.kodebase.messaging.query.QueryHandler
import org.kodebase.monitoring.Monitoring

class LocalQueryBus(
//    handlerProvider: () -> Set<QueryHandler<Query<Any>, Any>>,
    di: DI,
    override val monitoring: Monitoring,
    override val middlewares: Set<MessageBusMiddleware>,
    override val failureHandler: FailureHandler<Query<Any>, Any, ApplicationException> = ApplicationFailureHandler()
) : LocalMessageBus<Query<Any>, Any>(), QueryBus {
    override val handlers: Set<QueryHandler<Query<Any>, Any>> by di.instance()
}
