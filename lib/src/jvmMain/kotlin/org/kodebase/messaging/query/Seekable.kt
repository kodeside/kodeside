package org.kodebase.messaging.query

import org.kodebase.common.type.ID

interface Seekable {
    val seekId: ID?
    val limit: Int
    val ascending: Boolean
}