package org.kodebase.messaging.repository

import org.kodebase.common.repository.EntityRepository
import org.kodebase.messaging.model.EventLog

interface EventLogRepository<F> : EntityRepository<EventLog, F>