package org.kodebase.messaging.repository

import com.mongodb.client.MongoDatabase
import org.bson.conversions.Bson
import org.kodebase.common.repository.MongoEntityRepository
import org.kodebase.messaging.model.EventLog

class MongoEventLogRepository(database: MongoDatabase) : EventLogRepository<Bson>,
    MongoEntityRepository<EventLog>(database, EventLog::class)