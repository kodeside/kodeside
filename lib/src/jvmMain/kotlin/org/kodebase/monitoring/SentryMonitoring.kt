package org.kodebase.monitoring

import org.kodebase.monitoring.performance.PerformanceTransaction
import org.kodebase.monitoring.performance.SentryTransaction
import io.sentry.Sentry
import io.sentry.SentryEvent
import io.sentry.SentryOptions
import io.sentry.protocol.Message
import org.kodebase.security.UserPrincipal

actual class SentryMonitoring actual constructor(
    dsn: String?,
    release: String?,
    environment: String?,
    tracesSampleRate: Double,
    isTraceSampling: Boolean
) : AbstractMonitoring() {
    private var forceLogging: Boolean = false
    private var persistentTransactions: MutableMap<Pair<String, String>, PerformanceTransaction> = mutableMapOf()

    init {
        if (dsn != null) {
            val options = SentryOptions()
            options.dsn = dsn
            options.release = release
            options.environment = environment
            options.tracesSampleRate = tracesSampleRate
            options.isTraceSampling = isTraceSampling
            options.tracesSampler = SentryOptions.TracesSamplerCallback { context ->
                if (forceLogging) {
                    return@TracesSamplerCallback 1.0
                } else {
                    // Drop this transaction, by setting its sample rate to 0%
                    return@TracesSamplerCallback 0.1
                }
            }
            Sentry.init(options)
        }
    }

    override fun capture(text: String?, error: Throwable?, tags: Map<String, String>) {
        try {
            with(SentryEvent()) {
                if (text != null)
                    message = Message().apply {
                        message = text
                    }
                if (error != null)
                    throwable = error
                tags.forEach { (key, value) ->
                    setTag(key, value)
                }
                Sentry.captureEvent(this)
            }
        } catch (t: Throwable) {
            logger.error("Sentry monitoring error: ${t.message}", t)
        }
    }

    override fun message(text: String?) {
        try {
            Sentry.captureMessage(text?: "")
        } catch (t: Throwable) {
            logger.error("Sentry monitoring error: ${t.message}", t)
        }
    }

    override fun createPerformanceTransaction(
        name: String,
        operation: String,
        userPrincipal: UserPrincipal?,
        tags: MutableMap<String, String>,
        persistent: Boolean
    ): PerformanceTransaction {
        return if (persistent) {
            persistentTransactions[Pair(name, operation)] = SentryTransaction(name, operation, userPrincipal, tags)
            getPerformanceTransaction(name, operation)!!
        } else {
            SentryTransaction(name, operation, userPrincipal, tags)
        }
    }

    override fun getPerformanceTransaction(name: String, operation: String): PerformanceTransaction? =
        persistentTransactions[Pair(name,operation)]


    override fun removePerformanceTransaction(name: String, operation: String) {
        persistentTransactions.remove(Pair(name, operation))
    }

    override fun setForceLogging(toValue: Boolean) {
        forceLogging = toValue
    }
}