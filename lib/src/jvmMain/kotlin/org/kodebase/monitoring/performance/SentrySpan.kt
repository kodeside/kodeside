package org.kodebase.monitoring.performance

import io.sentry.ISpan
import io.sentry.SpanStatus

open class SentrySpan(
    override val parent: SentrySpan?,
    override val operation: String,
    override val tags: MutableMap<String, String>,
    override val children: MutableList<PerformanceSpan> = mutableListOf()
) : PerformanceSpan {

    lateinit var span: ISpan

    override fun createChild(operation: String, tags: MutableMap<String, String>) : SentrySpan {
        return SentrySpan(this, operation, tags).also {
            children.add(it)
        }
    }

    override fun start() {
        val parent = parent ?: error("Missing parent span")
        span = parent.span.startChild(operation)
        span.status = SpanStatus.OK
    }

    override fun abort() = performSpanOperation {
        span.status = SpanStatus.UNKNOWN_ERROR
    }

    override fun abort(e: Throwable) = performSpanOperation {
        span.throwable = e
        span.status = SpanStatus.INTERNAL_ERROR
    }

    override fun finish() = performSpanOperation {
        tags.forEach { (key, value) ->
            span.setTag(key, value)
        }
        span.finish()
    }

    private fun performSpanOperation(block: () -> Unit){
        if (this::span.isInitialized) {
            block()
        }
    }
}