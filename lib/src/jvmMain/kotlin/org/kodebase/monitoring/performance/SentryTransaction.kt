package org.kodebase.monitoring.performance

import io.sentry.Sentry
import io.sentry.SentryTraceHeader
import io.sentry.SpanStatus
import io.sentry.TransactionContext
import org.kodebase.security.UserPrincipal
import io.sentry.protocol.User

class SentryTransaction(
    override val name: String,
    operation: String,
    override val userPrincipal: UserPrincipal?,
    tags: MutableMap<String, String>
) : PerformanceTransaction, SentrySpan(null, operation, tags) {

    init {
        Sentry.configureScope {
            if (userPrincipal != null) {
                it.user = User().apply {
                    id = userPrincipal.id.toString()
                    username = userPrincipal.name
                    //others = user.others
                }
            }
        }
    }

    override val trace get() = span.toSentryTrace().value

    override fun start(trace: String?) {
       if (trace != null) {
           span = Sentry.startTransaction(TransactionContext.fromSentryTrace(name, operation, SentryTraceHeader(trace)))
           span.status = SpanStatus.OK
       } else {
           start()
       }
    }

    override fun start() {
        span = Sentry.startTransaction(name, operation)
        span.status = SpanStatus.OK
    }
}