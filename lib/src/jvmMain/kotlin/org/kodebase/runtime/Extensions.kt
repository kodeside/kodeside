package org.kodebase.runtime

actual fun getenv(name: String): String? = System.getenv(name)