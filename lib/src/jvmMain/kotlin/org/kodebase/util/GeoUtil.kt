package org.kodebase.util

import org.kodebase.common.type.CoordinateType
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.math.abs

object GeoUtil {

    private fun String.toInput() = removeWhitespaces().replace('´', '\'').replace(',', '.')

    private fun Matcher.toDouble(): Double {
        val sign = if ("" == group(1)) 1 else -1
        val degrees: Double = group(2).toDouble()
        val minutes: Double = group(3).toDouble()
        val seconds: Double = group(4).toDouble()
        val direction = if (listOf("W", "S").contains(group(5))) -1 else 1
        return sign * direction * (degrees + minutes / 60 + seconds / 3600)
    }

    fun String.toCoordinate(type: CoordinateType): Double {
        val input = toInput()
        var m = Pattern.compile("([\\-\\d.]+)([NSEW]?)").matcher(input)
        return if (m.matches()) {
            m.group(1).toDouble() * (if (listOf("W", "S").contains(m.group(2))) -1 else 1)
        } else {
            m = Pattern.compile("(-?)(\\d+)°(\\d+)'([\\d.]+)\"([NSEW])").matcher(input)
            if (m.matches()) {
                val coordinate = m.toDouble()
                if (abs(coordinate) > type.limit)
                    throw NumberFormatException("Invalid ${type.name.lowercase()}: $coordinate")
                coordinate
            } else {
                throw NumberFormatException("Malformed degrees/minutes/seconds/direction ${type.name.lowercase()}: $this")
            }
        }
    }

    fun String.toLatitude() = toCoordinate(CoordinateType.Latitude)

    fun String.toLongitude() = toCoordinate(CoordinateType.Longitude)

    @JvmStatic
    fun main(args: Array<String>) {
        println("14,43028649777898 ".toLongitude())
        println(" 49.5 E".toLatitude())
        println(" 49°47'37,015\" N".toLatitude())
        println(" -45,001 ".toLatitude())
    }
}