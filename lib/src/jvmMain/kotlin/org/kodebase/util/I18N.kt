package org.kodebase.util

import java.util.*

actual class I18N(private val translations: Map<String, Any>) {

    private fun translate(text: String, language: String, map: Map<*, *>): String {
        val p = text.indexOf('.')
        return if (p >= 0) {
            val value = map[text.substring(0, p)]
            if (value is Map<*, *>)
                translate(text.substring(p + 1), language, value)
            else
                (value as? String) ?: text
        } else {
            (map[text] as? String) ?: text
        }
    }

    actual fun translate(text: String, language: String) =
        (translations[language] ?: translations["en"])?.let { translate(text, language, it as Map<*, *>) } ?: text

    actual fun format(text: String, language: String, vararg args: Any?) =
        translate(text.replace("%", "%%"), language).format(Locale(language), *args)
}