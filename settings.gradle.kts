pluginManagement {
    resolutionStrategy {
        eachPlugin {
            when {
                requested.id.id == "kotlinx-serialization" -> useModule("org.jetbrains.kotlin:kotlin-serialization:${requested.version}")
            }
        }
    }
}

plugins {
    id("com.gradle.enterprise") version "3.6.3"
}

//gradleEnterprise {
//    buildScan {
//        termsOfServiceUrl = "https://gradle.com/terms-of-service"
//        termsOfServiceAgree = "yes"
//    }
//}

rootProject.name = "kodebase"

arrayOf(
    "lib",
    "examples"
).forEach {
    val projectPath = ":${rootProject.name}-${it.replace('/', '-')}"
    include(projectPath)
    project(projectPath).projectDir = file(it)
}